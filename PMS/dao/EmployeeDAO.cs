﻿using PMS.model;
using PMS.util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.dao
{
    class EmployeeDAO
    {
        string connectionString = ConfigurationManager.
            ConnectionStrings["PMS"].ConnectionString;

        public bool CheckLogin(Employee emp)
        {
            bool check = false;
            string sql = @"SELECT username FROM Employee WHERE username = @username AND password = @password";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@username", emp.Username);
                cmd.Parameters.AddWithValue("@password", emp.Password);

                conn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    check = dr.Read();
                }
            }
            return check;
        }

        public Employee SelectBySSN(string sSN)
        {
            Employee emp = null;
            string sql = @"SELECT * FROM Employee WHERE SSN = @SSN";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@SSN", sSN);

                conn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        string username = dr["username"].ToString();
                        string image = dr["image"].ToString();
                        image = ConstUtil.EMPLOYEE_IMAGE + image;
                        int roleID = int.Parse(dr["roleID"].ToString());
                        emp = new Employee(username, "", image, roleID, sSN);
                    }
                }
            }
            return emp;
        }

        public Employee SelectByUsername(string username)
        {
            Employee emp = null;
            string sql = @"SELECT * FROM Employee WHERE username = @username";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@username", username);

                conn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        string image = dr["image"].ToString();
                        image = ConstUtil.EMPLOYEE_IMAGE + image;
                        int roleID = int.Parse(dr["roleID"].ToString());
                        string sSN = dr["SSN"].ToString();
                        emp = new Employee(username, "", image, roleID, sSN);
                    }
                }
            }
            return emp;
        }

        public string CreateUsername()
        {
            string username;
            string sql = @"SELECT TOP (1) username FROM Employee ORDER BY username DESC";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                username = cmd.ExecuteScalar().ToString();
                int roll = int.Parse(username.Substring(2)) + 1;

                StringBuilder sb = new StringBuilder(roll.ToString());
                while (sb.Length < 5)
                {
                    sb.Insert(0, '0');
                }
                username = sb.Insert(0, "SE").ToString();
            }
            return username;
        }

        public void AddEmployee(Employee emp)
        {
            string sql = @"INSERT INTO Employee VALUES (@username, @password, @image, @roleID, @SSN)";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@username", emp.Username);
                cmd.Parameters.AddWithValue("@password", emp.Password);
                cmd.Parameters.AddWithValue("@image", emp.Image);
                cmd.Parameters.AddWithValue("@roleID", emp.RoleID);
                cmd.Parameters.AddWithValue("@SSN", emp.SSN);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void ChangePassword(Employee emp)
        {
            string sql = @"UPDATE Employee SET password = @password WHERE username = @username";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@password", emp.Password);
                cmd.Parameters.AddWithValue("@username", emp.Username);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateEmployee(Employee emp)
        {
            string sql = @"UPDATE Employee SET image = @image, roleID = @roleID WHERE username = @username";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@image", emp.Image);
                cmd.Parameters.AddWithValue("@roleID", emp.RoleID);
                cmd.Parameters.AddWithValue("@username", emp.Username);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
