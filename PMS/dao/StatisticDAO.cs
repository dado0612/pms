﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.dao
{
    class StatisticDAO
    {
        string connectionString = ConfigurationManager.
            ConnectionStrings["PMS"].ConnectionString;

        private DataTable FillData(string sql)
        {
            SqlDataAdapter da = new SqlDataAdapter(sql, connectionString);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public DataTable StasTrackByWeek()
        {
            string sql = @"SELECT CONCAT(DAY(time), '/', MONTH(time), '/', YEAR(time)) AS time,
                            COUNT(CASE WHEN status = 0 THEN 1 END) AS parkIn, COUNT(CASE WHEN
                        status = 1 THEN 1 END) AS parkOut FROM Track WHERE DATEDIFF(DAY, time,
                        GETDATE()) < 7 GROUP BY DAY(time), MONTH(time), YEAR(time) ORDER BY time";
            return FillData(sql);
        }

        public DataTable StasTrackByMonth()
        {
            string sql = @"SELECT CONCAT(MONTH(time), '/', YEAR(time)) AS time, COUNT(*) AS parkIn,
                            0 AS parkOut FROM Track GROUP BY MONTH(time), YEAR(time) ORDER BY time";
            return FillData(sql);
        }

        public DataTable StasCardStatus()
        {
            string sql = @"SELECT CASE WHEN (plateNumber != '' AND expireDate < GETDATE()) THEN
                            'InUsed' WHEN (plateNumber != '' AND expireDate > GETDATE()) THEN
                            'Expire' ELSE 'Unregister' END AS title, COUNT(*) AS number FROM Card
                            GROUP BY CASE WHEN (plateNumber != '' AND expireDate < GETDATE())
                            THEN 'InUsed' WHEN (plateNumber != '' AND expireDate > GETDATE())
                            THEN 'Expire' ELSE 'Unregister' END";
            return FillData(sql);
        }
    }
}
