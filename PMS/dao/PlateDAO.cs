﻿using PMS.model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.dao
{
    class PlateDAO
    {
        string connectionString = ConfigurationManager.
            ConnectionStrings["PMS"].ConnectionString;

        public List<string> SearchBySSN(string sSN)
        {
            List<string> list = new List<string>();
            string sql = @"SELECT plateNumber FROM Plate WHERE SSN = @SSN";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@SSN", sSN);

                conn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        list.Add(dr["plateNumber"].ToString());
                    }
                }
            }
            return list;
        }

        public void AddPlate(Plate plate)
        {
            string sql = @"INSERT INTO Plate VALUES (@plateNumber, @SSN)";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@plateNumber", plate.PlateNumber);
                cmd.Parameters.AddWithValue("@SSN", plate.SSN);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void DeletePlate(string plateNumber)
        {
            string sql = @"DELETE FROM Plate WHERE plateNumber = @plateNumber";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@plateNumber", plateNumber);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
