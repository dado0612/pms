﻿using PMS.model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.dao
{
    class ReportDAO
    {
        string connectionString = ConfigurationManager.
            ConnectionStrings["PMS"].ConnectionString;

        public List<Report> SelectByMonth(string name)
        {
            List<Report> list = new List<Report>();
            string sql = @"SELECT * FROM Report WHERE DATEDIFF(DAY, time, GETDATE()) < 31";
            if (!name.Equals(""))
            {
                sql += " AND username = '" + name + "'";
            }
            sql += " ORDER BY reportID DESC";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        int reportID = int.Parse(dr["reportID"].ToString());
                        long trackID = long.Parse(dr["trackID"].ToString());
                        string title = dr["title"].ToString();
                        string content = dr["content"].ToString();
                        DateTime time = DateTime.Parse(dr["time"].ToString());
                        string username = dr["username"].ToString();
                        list.Add(new Report(reportID, trackID, title, content, time, username));
                    }
                }
            }
            return list;
        }

        public Report SelectByID(int reportID)
        {
            Report rep = null;
            string sql = @"SELECT * FROM Report WHERE reportID = @reportID";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@reportID", reportID);

                conn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        long trackID = long.Parse(dr["trackID"].ToString());
                        string title = dr["title"].ToString();
                        string content = dr["content"].ToString();
                        DateTime time = DateTime.Parse(dr["time"].ToString());
                        string username = dr["username"].ToString();
                        rep = new Report(reportID, trackID, title, content, time, username);
                    }
                }
            }
            return rep;
        }

        public void AddReport(Report rep)
        {
            string sql = @"INSERT INTO Report VALUES (@trackID, @title, @content, @time, @username)";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@trackID", rep.TrackID);
                cmd.Parameters.AddWithValue("@title", rep.Title);
                cmd.Parameters.AddWithValue("@content", rep.Content);
                cmd.Parameters.AddWithValue("@time", rep.Time);
                cmd.Parameters.AddWithValue("@username", rep.Username);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
