﻿using PMS.model;
using PMS.util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.dao
{
    class ParkingDAO
    {
        string connectionString = ConfigurationManager.
            ConnectionStrings["PMS"].ConnectionString;

        public Parking SelectByID(int parkID)
        {
            Parking park = null;
            string sql = @"SELECT * FROM Parking WHERE parkID = @parkID";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@parkID", parkID);

                conn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        string parkName = dr["parkName"].ToString();
                        int maxPlace = int.Parse(dr["maxPlace"].ToString());
                        park = new Parking(parkName, maxPlace);
                    }
                }
            }

            long cIn = new TrackDAO().CountIO(parkID, (int)STATUS.IN);
            long cOut = new TrackDAO().CountIO(parkID, (int)STATUS.OUT);
            park.MaxPlace += (int)(cOut - cIn);
            return park;
        }

        public void AddParking(Parking park)
        {
            string sql = @"INSERT INTO Parking VALUES (@parkName, @maxPlace)";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@parkName", park.ParkName);
                cmd.Parameters.AddWithValue("@maxPlace", park.MaxPlace);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
