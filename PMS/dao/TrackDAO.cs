﻿using PMS.model;
using PMS.util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.dao
{
    class TrackDAO
    {
        string connectionString = ConfigurationManager.
            ConnectionStrings["PMS"].ConnectionString;

        public long CountIO(int parkID, int status)
        {
            long cnt = 0;
            string sql = @"SELECT COUNT(*) FROM Track WHERE parkID = @parkID AND status = @status";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@parkID", parkID);
                cmd.Parameters.AddWithValue("@status", status);

                conn.Open();
                cnt = long.Parse(cmd.ExecuteScalar().ToString());
            }
            return cnt;
        }

        public List<Track> SelectTop(int parkID)
        {
            List<Track> list = new List<Track>();
            string sql = @"SELECT TOP (18) * FROM Track WHERE parkID = @parkID ORDER BY trackID DESC";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@parkID", parkID);

                conn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string plateNumber = dr["plateNumber"].ToString();
                        bool stas = Convert.ToBoolean(dr["status"].ToString());
                        int status = stas ? (int)STATUS.OUT : (int)STATUS.IN;
                        DateTime time = DateTime.Parse(dr["time"].ToString());
                        string username = dr["username"].ToString();
                        list.Add(new Track(plateNumber, status, time, "", 0, username));
                    }
                }
            }
            return list;
        }

        public List<Track> SelectByPlateNumber(string plateNumber)
        {
            List<Track> list = new List<Track>();
            string sql = @"SELECT TOP (2) * FROM Track WHERE plateNumber = @plateNumber ORDER BY trackID DESC";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@plateNumber", plateNumber);

                conn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        long trackID = long.Parse(dr["trackID"].ToString());
                        bool stas = Convert.ToBoolean(dr["status"].ToString());
                        int status = stas ? (int)STATUS.OUT : (int)STATUS.IN;
                        DateTime time = DateTime.Parse(dr["time"].ToString());
                        string image = dr["image"].ToString();
                        image = ConstUtil.TRACK_IMAGE + image;
                        int parkID = int.Parse(dr["parkID"].ToString());
                        string username = dr["username"].ToString();
                        list.Add(new Track(trackID, plateNumber, status, time, image, parkID, username));
                    }
                }
            }

            if (list.Count == 2 && list.ElementAt(0).Status == (int)STATUS.IN)
            {
                list.RemoveAt(1);
            }
            return list;
        }

        public long GetMaxTrackID()
        {
            long cnt = 0;
            string sql = @"SELECT MAX(trackID) FROM Track";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                cnt = long.Parse(cmd.ExecuteScalar().ToString());
            }
            return cnt + 1;
        }

        public void AddTrack(Track track)
        {
            string sql = @"INSERT INTO Track VALUES (@plateNumber, @status, @time, @image, @parkID, @username)";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@plateNumber", track.PlateNumber);
                cmd.Parameters.AddWithValue("@status", track.Status);
                cmd.Parameters.AddWithValue("@time", track.Time);
                cmd.Parameters.AddWithValue("@image", track.Image);
                cmd.Parameters.AddWithValue("@parkID", track.ParkID);
                cmd.Parameters.AddWithValue("@username", track.Username);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
