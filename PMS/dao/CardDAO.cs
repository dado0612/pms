﻿using PMS.model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.dao
{
    class CardDAO
    {
        string connectionString = ConfigurationManager.
            ConnectionStrings["PMS"].ConnectionString;

        public Card SelectByID(string cardID)
        {
            Card card = null;
            string sql = @"SELECT * FROM Card WHERE cardID = @cardID";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@cardID", cardID);

                conn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        int serialNumber = int.Parse(dr["serialNumber"].ToString());
                        string plateNumber = dr["plateNumber"].ToString();
                        DateTime expireDate = DateTime.Parse(dr["expireDate"].ToString());
                        card = new Card(cardID, serialNumber, plateNumber, expireDate);
                    }
                }
            }
            return card;
        }

        public void AddCard(Card card)
        {
            string sql = @"INSERT INTO Card VALUES (@cardID, @plateNumber, @expireDate)";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@cardID", card.CardID);
                cmd.Parameters.AddWithValue("@plateNumber", card.PlateNumber);
                cmd.Parameters.AddWithValue("@expireDate", card.ExpireDate);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateCard(Card card)
        {
            string sql = @"UPDATE Card SET plateNumber = @plateNumber,
                        expireDate = @expireDate WHERE cardID = @cardID";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@plateNumber", card.PlateNumber);
                cmd.Parameters.AddWithValue("@expireDate", card.ExpireDate);
                cmd.Parameters.AddWithValue("@cardID", card.CardID);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
