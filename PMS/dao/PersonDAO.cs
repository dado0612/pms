﻿using PMS.model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.dao
{
    class PersonDAO
    {
        string connectionString = ConfigurationManager.
            ConnectionStrings["PMS"].ConnectionString;

        public Person SelectByUsername(string username)
        {
            Person per = null;
            string sql = @"SELECT Person.* FROM Employee INNER JOIN Person ON
                        Employee.SSN = Person.SSN WHERE username = @username";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@username", username);

                conn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        string sSN = dr["SSN"].ToString();
                        string name = dr["name"].ToString();
                        bool gender = Convert.ToBoolean(dr["gender"].ToString());
                        DateTime dob = DateTime.Parse(dr["dob"].ToString());
                        string address = dr["address"].ToString();
                        string phone = dr["phone"].ToString();
                        per = new Person(sSN, name, gender, dob, address, phone);
                    }
                }
            }
            return per;
        }

        public Person SelectBySSN(string sSN)
        {
            Person per = null;
            string sql = @"SELECT * FROM Person WHERE SSN = @SSN";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@SSN", sSN);

                conn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        string name = dr["name"].ToString();
                        bool gender = Convert.ToBoolean(dr["gender"].ToString());
                        DateTime dob = DateTime.Parse(dr["dob"].ToString());
                        string address = dr["address"].ToString();
                        string phone = dr["phone"].ToString();
                        per = new Person(sSN, name, gender, dob, address, phone);
                    }
                }
            }
            return per;
        }

        public void AddPerson(Person per)
        {
            string sql = @"INSERT INTO Person VALUES (@SSN, @name, @gender, @dob, @address, @phone)";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@SSN", per.SSN);
                cmd.Parameters.AddWithValue("@name", per.Name);
                cmd.Parameters.AddWithValue("@gender", per.Gender);
                cmd.Parameters.AddWithValue("@dob", per.Dob);
                cmd.Parameters.AddWithValue("@address", per.Address);
                cmd.Parameters.AddWithValue("@phone", per.Phone);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void ChangePhone(string phone, string SSN)
        {
            string sql = @"UPDATE Person SET phone = @phone WHERE SSN = @SSN";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@phone", phone);
                cmd.Parameters.AddWithValue("@SSN", SSN);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdatePerson(Person per)
        {
            string sql = @"UPDATE Person SET name = @name, gender = @gender, dob = @dob,
                            address = @address, phone = @phone WHERE SSN = @SSN";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@name", per.Name);
                cmd.Parameters.AddWithValue("@gender", per.Gender);
                cmd.Parameters.AddWithValue("@dob", per.Dob);
                cmd.Parameters.AddWithValue("@address", per.Address);
                cmd.Parameters.AddWithValue("@phone", per.Phone);
                cmd.Parameters.AddWithValue("@SSN", per.SSN);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
