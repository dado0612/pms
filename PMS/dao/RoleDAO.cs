﻿using PMS.model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.dao
{
    class RoleDAO
    {
        string connectionString = ConfigurationManager.
            ConnectionStrings["PMS"].ConnectionString;

        public List<string> SelectAll()
        {
            List<string> list = new List<string>();
            string sql = @"SELECT roleName FROM Role";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        list.Add(dr["roleName"].ToString());
                    }
                }
            }
            return list;
        }

        public void AddRole(Role role)
        {
            string sql = @"INSERT INTO Role VALUES (@roleID, @roleName)";

            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@roleID", role.RoleID);
                cmd.Parameters.AddWithValue("@roleName", role.RoleName);

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
