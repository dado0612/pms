﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.model
{
    class Card
    {
        public string CardID { get; set; }
        public int SerialNumber { get; set; }
        public string PlateNumber { get; set; }
        public DateTime ExpireDate { get; set; }

        public Card(string cardID, string plateNumber, DateTime expireDate)
        {
            CardID = cardID;
            PlateNumber = plateNumber;
            ExpireDate = expireDate;
        }

        public Card(string cardID, int serialNumber, string plateNumber, DateTime expireDate)
        {
            CardID = cardID;
            SerialNumber = serialNumber;
            PlateNumber = plateNumber;
            ExpireDate = expireDate;
        }
    }
}
