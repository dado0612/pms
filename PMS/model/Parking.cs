﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.model
{
    class Parking
    {
        public string ParkName { get; set; }
        public int MaxPlace { get; set; }

        public Parking(string parkName, int maxPlace)
        {
            ParkName = parkName;
            MaxPlace = maxPlace;
        }
    }
}
