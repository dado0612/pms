﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.model
{
    class Report
    {
        public int ReportID { get; set; }
        public long TrackID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime Time { get; set; }
        public string Username { get; set; }

        public Report(long trackID, string title, string content, DateTime time, string username)
        {
            TrackID = trackID;
            Title = title;
            Content = content;
            Time = time;
            Username = username;
        }

        public Report(int reportID, long trackID, string title, string content, DateTime time, string username)
        {
            ReportID = reportID;
            TrackID = trackID;
            Title = title;
            Content = content;
            Time = time;
            Username = username;
        }
    }
}
