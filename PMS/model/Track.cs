﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.model
{
    class Track
    {
        public long TrackID { get; set; }
        public string PlateNumber { get; set; }
        public int Status { get; set; }
        public DateTime Time { get; set; }
        public string Image { get; set; }
        public int ParkID { get; set; }
        public string Username { get; set; }

        public Track(string plateNumber, int status, DateTime time, string image, int parkID, string username)
        {
            PlateNumber = plateNumber;
            Status = status;
            Time = time;
            Image = image;
            ParkID = parkID;
            Username = username;
        }

        public Track(long trackID, string plateNumber, int status, DateTime time, string image, int parkID, string username)
        {
            TrackID = trackID;
            PlateNumber = plateNumber;
            Status = status;
            Time = time;
            Image = image;
            ParkID = parkID;
            Username = username;
        }
    }
}
