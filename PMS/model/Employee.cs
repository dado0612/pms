﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.model
{
    class Employee
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Image { get; set; }
        public int RoleID { get; set; }
        public string SSN { get; set; }

        public Employee(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public Employee(string username, string password, string image, int roleID, string sSN)
        {
            Username = username;
            Password = password;
            Image = image;
            RoleID = roleID;
            SSN = sSN;
        }
    }
}
