﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.model
{
    class Person
    {
        public string SSN { get; set; }
        public string Name { get; set; }
        public bool Gender { get; set; }
        public DateTime Dob { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }

        public Person(string sSN, string name, bool gender, DateTime dob, string address, string phone)
        {
            SSN = sSN;
            Name = name;
            Gender = gender;
            Dob = dob;
            Address = address;
            Phone = phone;
        }
    }
}
