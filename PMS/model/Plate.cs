﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.model
{
    class Plate
    {
        public string PlateNumber { get; set; }
        public string SSN { get; set; }

        public Plate(string plateNumber, string sSN)
        {
            PlateNumber = plateNumber;
            SSN = sSN;
        }
    }
}
