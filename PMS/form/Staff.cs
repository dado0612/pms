﻿using PMS.dao;
using PMS.model;
using PMS.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebEye.Controls.WinForms.WebCameraControl;

namespace PMS.form
{
    public partial class Staff : Form
    {
        private int cntQR;
        private string SSN;
        private string cardID;
        private long trackID;
        private string username;

        public Staff()
        {
            InitializeComponent();
        }

        public Staff(string username)
        {
            this.username = username;
            InitializeComponent();
            dateTimePickerCus.MaxDate = DateTime.Now.AddYears(-18);
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabPage tab = (sender as TabControl).SelectedTab;
            switch (tab.Text)
            {
                case "Card":
                    OpenTabCard();
                    break;
                case "Profile":
                    OpenTabProfile();
                    break;
            }
        }

        // Customer tab
        private void textBoxSSN_TextChanged(object sender, EventArgs e)
        {
            ClearCustomer();
            string sSN = textBoxSSN.Text;
            if (sSN.Length == 12)
            {
                Person per = new PersonDAO().SelectBySSN(sSN);
                if (per == null)
                {
                    MessageBox.Show("Customer hasn't already existed.", "Warning",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    groupBoxCustomer.Text = "Update customer";
                    textBoxNameCus.Text = per.Name;
                    radioButtonMale.Checked = !per.Gender;
                    radioButtonFemale.Checked = per.Gender;
                    dateTimePickerCus.Value = per.Dob;
                    textBoxAddressCus.Text = per.Address;
                    textBoxPhoneCus.Text = per.Phone;

                    List<string> list = new PlateDAO().SearchBySSN(sSN);
                    int len = list.Count;
                    for (int i = 0; i < len; i++)
                    {
                        dataGridViewPlate.Rows.Add(list.ElementAt(i));
                        dataGridViewPlate.Rows[i].Cells[0].ReadOnly = true;
                    }
                }
            }
        }

        public void ClearCustomer()
        {
            groupBoxCustomer.Text = "Add customer";
            textBoxNameCus.Text = string.Empty;
            radioButtonMale.Checked = true;
            dateTimePickerCus.Value = dateTimePickerCus.MaxDate;
            textBoxAddressCus.Text = string.Empty;
            textBoxPhoneCus.Text = string.Empty;
            dataGridViewPlate.Rows.Clear();
        }

        private void dataGridViewPlate_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridViewPlate.Columns["delete"].Index && e.RowIndex >= 0)
            {
                string plate = dataGridViewPlate.Rows[e.RowIndex].Cells[0].Value.ToString();
                if (MessageBox.Show("Do you really want to delete this plate number?", "Warning",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    new PlateDAO().DeletePlate(plate);
                    MessageBox.Show("Delete plate successfully.", "Success",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dataGridViewPlate.Rows.RemoveAt(e.RowIndex);
                }
            }
        }

        private void dataGridViewPlate_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (MessageBox.Show("Do you really want to add this plate number?", "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                string sSN = textBoxSSN.Text;
                DataGridViewCell plate = dataGridViewPlate.Rows[e.RowIndex].Cells[0];
                new PlateDAO().AddPlate(new Plate(plate.Value.ToString(), sSN));
                MessageBox.Show("Add plate successfully.", "Success",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                plate.ReadOnly = true;
            }
        }

        private string ValidCustomer(Person per)
        {
            Dummy Name = new Dummy(per.Name, "Name");
            Dummy Address = new Dummy(per.Address, "Address");
            ValidUtil valid = new ValidUtil();

            string msg = valid.ValidSSN(per.SSN);
            if (msg == null)
            {
                msg = valid.ValidString(Name, Address);
            }
            if (msg == null)
            {
                msg = valid.ValidPhone(per.Phone);
            }

            return msg;
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            string sSN = textBoxSSN.Text;
            string name = textBoxNameCus.Text;
            bool gender = radioButtonFemale.Checked;
            DateTime dob = dateTimePickerCus.Value;
            string address = textBoxAddressCus.Text;
            string phone = textBoxPhoneCus.Text;
            Person per = new Person(sSN, name, gender, dob, address, phone);

            string msg = ValidCustomer(per);
            if (msg == null)
            {
                if (groupBoxCustomer.Text.StartsWith("Add"))
                {
                    new PersonDAO().AddPerson(per);
                    msg = "Add person successfully";
                }
                else
                {
                    new PersonDAO().UpdatePerson(per);
                    msg = "Update person successfully";
                }

                MessageBox.Show(msg, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBoxSSN.Text = string.Empty;
                ClearCustomer();
            }
            else
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Card tab
        public void OpenTabCard()
        {
            WebCameraId id = new LibUtil().GetCamera(webcam);
            webcam.StartCapture(id);
            comboBoxRenew.SelectedIndex = 0;
        }

        private void buttonQR_Click(object sender, EventArgs e)
        {
            cardID = null;
            while (cardID == null)
            {
                string path = ConstUtil.TMP_IMAGE;
                path += "qr" + ++cntQR + ".bmp";
                webcam.GetCurrentImage().Save(path);
                cardID = new LibUtil().ReadQR(path);
            }
            LoadQR();
        }

        private void LoadQR()
        {
            Card card = new CardDAO().SelectByID(cardID);
            textBoxPlateQR.Text = card.PlateNumber;
            textBoxSerial.Text = card.SerialNumber.ToString();
            textBoxExpire.Text = card.ExpireDate.ToString();
            buttonUpdateCard.Enabled = true;
        }

        private void buttonUpdateCard_Click(object sender, EventArgs e)
        {
            string plate = textBoxPlateQR.Text;
            Dummy Plate = new Dummy(plate, "Plate number");

            string msg = new ValidUtil().ValidString(Plate);
            if (msg == null)
            {
                DateTime expire = DateTime.Parse(textBoxExpire.Text);
                if (expire < DateTime.Now)
                {
                    expire = DateTime.Now;
                }
                expire = expire.AddMonths(comboBoxRenew.SelectedIndex + 1);

                new CardDAO().UpdateCard(new Card(cardID, plate, expire));
                MessageBox.Show("Register card successfully.", "Success",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                LoadQR();
                buttonUpdateCard.Enabled = false;
            }
            else
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Report tab
        private void textBoxPlate_TextChanged(object sender, EventArgs e)
        {
            string plate = textBoxPlate.Text;
            List<Track> list = new TrackDAO().SelectByPlateNumber(plate);

            if (list.Count == 0)
            {
                ClearReport();
            }
            else
            {
                pictureBoxIn.ImageLocation = list.ElementAt(0).Image;
                trackID = list.ElementAt(0).TrackID;

                if (list.Count == 2)
                {
                    pictureBoxOut.ImageLocation = list.ElementAt(1).Image;
                }
            }
        }

        public void ClearReport()
        {
            textBoxTitle.Text = string.Empty;
            textBoxContent.Text = string.Empty;
            pictureBoxIn.ImageLocation = null;
            pictureBoxOut.ImageLocation = null;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            string plate = textBoxPlate.Text;
            Dummy Plate = new Dummy(plate, "Plate");
            string title = textBoxTitle.Text;
            Dummy Title = new Dummy(title, "Title");
            string content = textBoxContent.Text;
            Dummy Content = new Dummy(content, "Content");

            string msg = new ValidUtil().ValidString(Plate, Title, Content);
            if (msg == null)
            {
                DateTime time = DateTime.Now;
                Report rep = new Report(trackID, title, content, time, username);
                new ReportDAO().AddReport(rep);

                MessageBox.Show("Add report successfully.", "Success",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBoxPlate.Text = string.Empty;
                ClearReport();
            }
            else
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Profile tab
        public void OpenTabProfile()
        {
            Person per = new PersonDAO().SelectByUsername(username);
            Employee emp = new EmployeeDAO().SelectByUsername(username);
            this.SSN = per.SSN;

            pictureBoxAvatar.ImageLocation = emp.Image;
            textBoxNamePro.Text = per.Name;
            textBoxGenderPro.Text = (per.Gender) ? "Female" : "Male";
            dateTimePickerPro.Value = per.Dob;
            textBoxPhonePro.Text = per.Phone;
            textBoxAddressPro.Text = per.Address;
            textBoxPosition.Text = "Staff";
            textBoxUsername.Text = username;
        }

        private void buttonUpdateProfile_Click(object sender, EventArgs e)
        {
            string phone = textBoxPhonePro.Text;

            string msg = new ValidUtil().ValidPhone(phone);
            if (msg == null)
            {
                new PersonDAO().ChangePhone(phone, SSN);
                MessageBox.Show("Update successfully.", "Success",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string ValidPassword()
        {
            string oldPass = textBoxOldPass.Text;
            oldPass = new EncodeUtil().EncryptPassword(oldPass);
            Employee emp = new Employee(username, oldPass);

            string msg = null;
            if (new EmployeeDAO().CheckLogin(emp))
            {
                string newPass = textBoxNewPass.Text;
                string rePass = textBoxRePass.Text;
                if (!newPass.Equals(rePass))
                {
                    msg = "Re-password doesn't match with new password.";
                }
            }
            else
            {
                msg = "Old password is incorrect.";
            }

            return msg;
        }

        private void buttonChangePass_Click(object sender, EventArgs e)
        {
            string msg = ValidPassword();
            if (msg == null)
            {
                string newPass = textBoxNewPass.Text;
                string password = new EncodeUtil().EncryptPassword(newPass);
                new EmployeeDAO().ChangePassword(new Employee(username, password));
                MessageBox.Show("Change password successfully.",
                    "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            ClearPassword();
        }

        private void ClearPassword()
        {
            textBoxOldPass.Text = string.Empty;
            textBoxNewPass.Text = string.Empty;
            textBoxRePass.Text = string.Empty;
        }
    }
}
