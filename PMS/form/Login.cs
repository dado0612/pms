﻿using PMS.dao;
using PMS.model;
using PMS.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PMS.form
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void CheckLogin(Employee emp, string username)
        {
            if (new EmployeeDAO().CheckLogin(emp))
            {
                emp = new EmployeeDAO().SelectByUsername(username);
                if (emp.RoleID == (int)ROLE.RETIRE)
                {
                    labelMessage.Text = "You are retired. Please contact to your manager for";
                    labelMessage.Text += "\ngetting more information.";
                }
                else
                {
                    this.Hide();
                    Form form;
                    switch (emp.RoleID)
                    {
                        case (int)ROLE.SECURITY:
                            form = new Security(username);
                            break;
                        case (int)ROLE.STAFF:
                            form = new Staff(username);
                            break;
                        default:
                            form = new Manager(username);
                            break;
                    }
                    form.FormClosed += Form_FormClosed;
                    form.Show();
                }
            }
            else
            {
                labelMessage.Text = "Wrong username or password. Please try again.";
            }
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            string username = textBoxUsername.Text;
            Dummy Username = new Dummy(username, "Username");
            string password = textBoxPassword.Text;
            Dummy Password = new Dummy(password, "Password");

            string msg = new ValidUtil().ValidString(Username, Password);
            if (msg == null)
            {
                password = new EncodeUtil().EncryptPassword(password);
                CheckLogin(new Employee(username, password), username);
                textBoxPassword.Text = string.Empty;
            }
            else
            {
                labelMessage.Text = msg;
            }
        }

        private void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            textBoxUsername.Text = string.Empty;
            labelMessage.Text = string.Empty;
            this.Show();
        }
    }
}
