﻿using WebEye.Controls.WinForms.WebCameraControl;

namespace PMS.form
{
    partial class Security
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxCard = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxSerial = new System.Windows.Forms.TextBox();
            this.textBoxPlateQR = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxExpire = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBoxTrack = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxTime = new System.Windows.Forms.TextBox();
            this.textBoxPlateANPR = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxStatus = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBoxGeneral = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxFree = new System.Windows.Forms.TextBox();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxMax = new System.Windows.Forms.TextBox();
            this.textBoxParking = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonQR = new System.Windows.Forms.Button();
            this.buttonANPR = new System.Windows.Forms.Button();
            this.webcam = new WebEye.Controls.WinForms.WebCameraControl.WebCameraControl();
            this.securityID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.plateNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBoxCard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBoxTrack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBoxGeneral.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.groupBoxCard, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxTrack, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxGeneral, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1532, 853);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBoxCard
            // 
            this.groupBoxCard.Controls.Add(this.label8);
            this.groupBoxCard.Controls.Add(this.textBoxSerial);
            this.groupBoxCard.Controls.Add(this.textBoxPlateQR);
            this.groupBoxCard.Controls.Add(this.label6);
            this.groupBoxCard.Controls.Add(this.textBoxExpire);
            this.groupBoxCard.Controls.Add(this.label7);
            this.groupBoxCard.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxCard.Location = new System.Drawing.Point(1024, 482);
            this.groupBoxCard.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxCard.Name = "groupBoxCard";
            this.groupBoxCard.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxCard.Size = new System.Drawing.Size(504, 367);
            this.groupBoxCard.TabIndex = 14;
            this.groupBoxCard.TabStop = false;
            this.groupBoxCard.Text = "Card Info";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(50, 70);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 20);
            this.label8.TabIndex = 8;
            this.label8.Text = "Serial Number";
            // 
            // textBoxSerial
            // 
            this.textBoxSerial.Location = new System.Drawing.Point(175, 67);
            this.textBoxSerial.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxSerial.Name = "textBoxSerial";
            this.textBoxSerial.ReadOnly = true;
            this.textBoxSerial.Size = new System.Drawing.Size(250, 27);
            this.textBoxSerial.TabIndex = 9;
            // 
            // textBoxPlateQR
            // 
            this.textBoxPlateQR.Location = new System.Drawing.Point(175, 127);
            this.textBoxPlateQR.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxPlateQR.Name = "textBoxPlateQR";
            this.textBoxPlateQR.ReadOnly = true;
            this.textBoxPlateQR.Size = new System.Drawing.Size(250, 27);
            this.textBoxPlateQR.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(50, 190);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "Expire Date";
            // 
            // textBoxExpire
            // 
            this.textBoxExpire.Location = new System.Drawing.Point(175, 187);
            this.textBoxExpire.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxExpire.Name = "textBoxExpire";
            this.textBoxExpire.ReadOnly = true;
            this.textBoxExpire.Size = new System.Drawing.Size(250, 27);
            this.textBoxExpire.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(50, 130);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 20);
            this.label7.TabIndex = 10;
            this.label7.Text = "Plate Number";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.plateNumber,
            this.status,
            this.time,
            this.securityID});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(1025, 5);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(5);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(502, 416);
            this.dataGridView1.TabIndex = 0;
            // 
            // groupBoxTrack
            // 
            this.groupBoxTrack.Controls.Add(this.label5);
            this.groupBoxTrack.Controls.Add(this.buttonSave);
            this.groupBoxTrack.Controls.Add(this.label9);
            this.groupBoxTrack.Controls.Add(this.textBoxTime);
            this.groupBoxTrack.Controls.Add(this.textBoxPlateANPR);
            this.groupBoxTrack.Controls.Add(this.label10);
            this.groupBoxTrack.Controls.Add(this.textBoxStatus);
            this.groupBoxTrack.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxTrack.Location = new System.Drawing.Point(4, 482);
            this.groupBoxTrack.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTrack.Name = "groupBoxTrack";
            this.groupBoxTrack.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTrack.Size = new System.Drawing.Size(502, 367);
            this.groupBoxTrack.TabIndex = 22;
            this.groupBoxTrack.TabStop = false;
            this.groupBoxTrack.Text = "Plate Number Info";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(50, 70);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 20);
            this.label5.TabIndex = 15;
            this.label5.Text = "Plate Number";
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(300, 247);
            this.buttonSave.Margin = new System.Windows.Forms.Padding(5);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(125, 35);
            this.buttonSave.TabIndex = 21;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(50, 130);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 20);
            this.label9.TabIndex = 17;
            this.label9.Text = "Status";
            // 
            // textBoxTime
            // 
            this.textBoxTime.Location = new System.Drawing.Point(175, 187);
            this.textBoxTime.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxTime.Name = "textBoxTime";
            this.textBoxTime.ReadOnly = true;
            this.textBoxTime.Size = new System.Drawing.Size(250, 27);
            this.textBoxTime.TabIndex = 20;
            // 
            // textBoxPlateANPR
            // 
            this.textBoxPlateANPR.Location = new System.Drawing.Point(175, 67);
            this.textBoxPlateANPR.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxPlateANPR.Name = "textBoxPlateANPR";
            this.textBoxPlateANPR.ReadOnly = true;
            this.textBoxPlateANPR.Size = new System.Drawing.Size(250, 27);
            this.textBoxPlateANPR.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(50, 190);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 20);
            this.label10.TabIndex = 19;
            this.label10.Text = "Time";
            // 
            // textBoxStatus
            // 
            this.textBoxStatus.Location = new System.Drawing.Point(175, 127);
            this.textBoxStatus.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxStatus.Name = "textBoxStatus";
            this.textBoxStatus.ReadOnly = true;
            this.textBoxStatus.Size = new System.Drawing.Size(250, 27);
            this.textBoxStatus.TabIndex = 18;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(515, 431);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(500, 417);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBoxGeneral
            // 
            this.groupBoxGeneral.Controls.Add(this.label1);
            this.groupBoxGeneral.Controls.Add(this.textBoxFree);
            this.groupBoxGeneral.Controls.Add(this.textBoxUsername);
            this.groupBoxGeneral.Controls.Add(this.label4);
            this.groupBoxGeneral.Controls.Add(this.label2);
            this.groupBoxGeneral.Controls.Add(this.textBoxMax);
            this.groupBoxGeneral.Controls.Add(this.textBoxParking);
            this.groupBoxGeneral.Controls.Add(this.label3);
            this.groupBoxGeneral.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxGeneral.Location = new System.Drawing.Point(4, 58);
            this.groupBoxGeneral.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxGeneral.Name = "groupBoxGeneral";
            this.groupBoxGeneral.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxGeneral.Size = new System.Drawing.Size(502, 364);
            this.groupBoxGeneral.TabIndex = 8;
            this.groupBoxGeneral.TabStop = false;
            this.groupBoxGeneral.Text = "General Info";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 70);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username";
            // 
            // textBoxFree
            // 
            this.textBoxFree.Location = new System.Drawing.Point(175, 247);
            this.textBoxFree.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxFree.Name = "textBoxFree";
            this.textBoxFree.ReadOnly = true;
            this.textBoxFree.Size = new System.Drawing.Size(250, 27);
            this.textBoxFree.TabIndex = 7;
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Location = new System.Drawing.Point(175, 67);
            this.textBoxUsername.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.ReadOnly = true;
            this.textBoxUsername.Size = new System.Drawing.Size(250, 27);
            this.textBoxUsername.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(50, 250);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Free Place";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 130);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Parking";
            // 
            // textBoxMax
            // 
            this.textBoxMax.Location = new System.Drawing.Point(175, 187);
            this.textBoxMax.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxMax.Name = "textBoxMax";
            this.textBoxMax.ReadOnly = true;
            this.textBoxMax.Size = new System.Drawing.Size(250, 27);
            this.textBoxMax.TabIndex = 5;
            // 
            // textBoxParking
            // 
            this.textBoxParking.Location = new System.Drawing.Point(175, 127);
            this.textBoxParking.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxParking.Name = "textBoxParking";
            this.textBoxParking.ReadOnly = true;
            this.textBoxParking.Size = new System.Drawing.Size(250, 27);
            this.textBoxParking.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 190);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Max Place";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonQR);
            this.panel1.Controls.Add(this.buttonANPR);
            this.panel1.Controls.Add(this.webcam);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(515, 5);
            this.panel1.Margin = new System.Windows.Forms.Padding(5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(500, 416);
            this.panel1.TabIndex = 1;
            // 
            // buttonQR
            // 
            this.buttonQR.Enabled = false;
            this.buttonQR.Location = new System.Drawing.Point(321, 355);
            this.buttonQR.Margin = new System.Windows.Forms.Padding(5);
            this.buttonQR.Name = "buttonQR";
            this.buttonQR.Size = new System.Drawing.Size(125, 35);
            this.buttonQR.TabIndex = 3;
            this.buttonQR.Text = "Scan QR";
            this.buttonQR.UseVisualStyleBackColor = true;
            this.buttonQR.Click += new System.EventHandler(this.buttonQR_Click);
            // 
            // buttonANPR
            // 
            this.buttonANPR.Location = new System.Drawing.Point(57, 355);
            this.buttonANPR.Margin = new System.Windows.Forms.Padding(5);
            this.buttonANPR.Name = "buttonANPR";
            this.buttonANPR.Size = new System.Drawing.Size(125, 35);
            this.buttonANPR.TabIndex = 2;
            this.buttonANPR.Text = "Scan ANPR";
            this.buttonANPR.UseVisualStyleBackColor = true;
            this.buttonANPR.Click += new System.EventHandler(this.buttonANPR_Click);
            // 
            // webcam
            // 
            this.webcam.Dock = System.Windows.Forms.DockStyle.Top;
            this.webcam.Location = new System.Drawing.Point(0, 0);
            this.webcam.Margin = new System.Windows.Forms.Padding(5);
            this.webcam.Name = "webcam";
            this.webcam.Size = new System.Drawing.Size(500, 327);
            this.webcam.TabIndex = 1;
            // 
            // securityID
            // 
            this.securityID.HeaderText = "Username";
            this.securityID.Name = "securityID";
            this.securityID.ReadOnly = true;
            // 
            // time
            // 
            this.time.HeaderText = "Time";
            this.time.Name = "time";
            this.time.ReadOnly = true;
            // 
            // status
            // 
            this.status.HeaderText = "Status";
            this.status.Name = "status";
            this.status.ReadOnly = true;
            // 
            // plateNumber
            // 
            this.plateNumber.HeaderText = "Plate Number";
            this.plateNumber.Name = "plateNumber";
            this.plateNumber.ReadOnly = true;
            // 
            // Security
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1532, 853);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "Security";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Security";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBoxCard.ResumeLayout(false);
            this.groupBoxCard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBoxTrack.ResumeLayout(false);
            this.groupBoxTrack.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBoxGeneral.ResumeLayout(false);
            this.groupBoxGeneral.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private WebCameraControl webcam;
        private System.Windows.Forms.Button buttonQR;
        private System.Windows.Forms.Button buttonANPR;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBoxSerial;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxExpire;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxPlateQR;
        private System.Windows.Forms.GroupBox groupBoxCard;
        private System.Windows.Forms.GroupBox groupBoxTrack;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxTime;
        private System.Windows.Forms.TextBox textBoxPlateANPR;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxStatus;
        private System.Windows.Forms.GroupBox groupBoxGeneral;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxFree;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxMax;
        private System.Windows.Forms.TextBox textBoxParking;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn plateNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn securityID;
    }
}