﻿using PMS.dao;
using PMS.model;
using PMS.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PMS.form
{
    public partial class Manager : Form
    {
        private string username;
        private string SSN;

        public Manager()
        {
            InitializeComponent();
        }

        public Manager(string username)
        {
            this.username = username;
            InitializeComponent();
            OpenTabStatistic();

            dateTimePickerEmp.MaxDate = DateTime.Now.AddYears(-18);
            openFileDialog1.InitialDirectory = ConstUtil.EMPLOYEE_IMAGE;

            List<string> listRole = new RoleDAO().SelectAll();
            foreach (string s in listRole)
            {
                comboBoxRole.Items.Add(s);
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabPage tab = (sender as TabControl).SelectedTab;
            switch (tab.Text)
            {
                case "Statistic":
                    OpenTabStatistic();
                    break;
                case "Report":
                    OpenTabReport("");
                    break;
                case "Profile":
                    OpenTabProfile();
                    break;
            }
        }

        // Statistic tab
        public void OpenTabStatistic()
        {
            comboBoxStatistic.SelectedIndex = 0;
            LoadTrack();
            LoadCard();
        }

        private void LoadTrack()
        {
            chartTrack.Update();
            if (comboBoxStatistic.SelectedIndex == 0)
            {
                chartTrack.DataSource = new StatisticDAO().StasTrackByWeek();
            }
            else
            {
                chartTrack.DataSource = new StatisticDAO().StasTrackByMonth();
            }

            chartTrack.Series["CheckIn"].XValueMember = "time";
            chartTrack.Series["CheckIn"].YValueMembers = "parkIn";
            chartTrack.Series["CheckOut"].XValueMember = "time";
            chartTrack.Series["CheckOut"].YValueMembers = "parkOut";
            chartTrack.DataBind();
        }

        private void LoadCard()
        {
            chartCard.DataSource = new StatisticDAO().StasCardStatus();
            chartCard.Series["SeriesCard"].XValueMember = "title";
            chartCard.Series["SeriesCard"].YValueMembers = "number";
            chartCard.DataBind();
        }

        private void comboBoxStatistic_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadTrack();
        }

        // Employee tab
        private void textBoxSSN_TextChanged(object sender, EventArgs e)
        {
            ClearEmployee();
            string sSN = textBoxSSN.Text;
            if (sSN.Length == 12)
            {
                Person per = new PersonDAO().SelectBySSN(sSN);
                Employee emp = new EmployeeDAO().SelectBySSN(sSN);
                if (per == null || emp == null)
                {
                    MessageBox.Show("Employee hasn't already existed.", "Warning",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    groupBoxEmployee.Text = "Update employee";
                    pictureBoxAvatarEmp.ImageLocation = emp.Image;
                    textBoxNameEmp.Text = per.Name;
                    radioButtonMale.Checked = !per.Gender;
                    radioButtonFemale.Checked = per.Gender;
                    dateTimePickerEmp.Value = per.Dob;
                    textBoxPhoneEmp.Text = per.Phone;
                    textBoxAddressEmp.Text = per.Address;
                    textBoxUsernameEmp.Text = emp.Username;
                    comboBoxRole.SelectedIndex = emp.RoleID;
                    buttonDone.Text = "Update employee";
                }
            }
        }

        public void ClearEmployee()
        {
            groupBoxEmployee.Text = "Add employee";
            pictureBoxAvatarEmp.ImageLocation = null;
            textBoxNameEmp.Text = string.Empty;
            radioButtonMale.Checked = true;
            dateTimePickerEmp.Value = dateTimePickerEmp.MaxDate;
            textBoxPhoneEmp.Text = string.Empty;
            textBoxAddressEmp.Text = string.Empty;
            textBoxUsernameEmp.Text = new EmployeeDAO().CreateUsername();
            comboBoxRole.SelectedIndex = 0;
            buttonDone.Text = "Add employee";
        }

        private void buttonImage_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBoxAvatarEmp.ImageLocation = openFileDialog1.FileName;
            }
        }

        private string ValidEmployee(Person per, Employee emp)
        {
            Dummy Name = new Dummy(per.Name, "Name");
            Dummy Address = new Dummy(per.Address, "Address");
            ValidUtil valid = new ValidUtil();

            string msg = valid.ValidSSN(per.SSN);
            if (msg == null)
            {
                msg = valid.ValidString(Name, Address);
            }
            if (msg == null)
            {
                msg = valid.ValidPhone(per.Phone);
            }
            if (msg == null)
            {
                msg = valid.ValidImage(emp.Image);
            }

            return msg;
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            string sSN = textBoxSSN.Text;
            string name = textBoxNameEmp.Text;
            bool gender = radioButtonFemale.Checked;
            DateTime dob = dateTimePickerEmp.Value;
            string address = textBoxAddressEmp.Text;
            string phone = textBoxPhoneEmp.Text;
            Person per = new Person(sSN, name, gender, dob, address, phone);

            string username = textBoxUsernameEmp.Text;
            string image = pictureBoxAvatarEmp.ImageLocation;
            int roleID = comboBoxRole.SelectedIndex;
            Employee emp = new Employee(username, string.Empty, image, roleID, sSN);

            string msg = ValidEmployee(per, emp);
            if (msg == null)
            {
                string[] arr = image.Split('\\');
                arr = arr[arr.Length - 1].Split('/');
                emp.Image = arr[arr.Length - 1];

                if (groupBoxEmployee.Text.StartsWith("Add"))
                {
                    string pass = ConstUtil.DEFAULT_PASSWORD;
                    emp.Password = new EncodeUtil().EncryptPassword(pass);
                    new PersonDAO().AddPerson(per);
                    new EmployeeDAO().AddEmployee(emp);
                    msg = "Add employee successfully.";
                }
                else
                {
                    new PersonDAO().UpdatePerson(per);
                    new EmployeeDAO().UpdateEmployee(emp);
                    msg = "Update employee successfully.";
                }

                MessageBox.Show(msg, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBoxSSN.Text = string.Empty;
                ClearEmployee();
            }
            else
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Report tab
        public void OpenTabReport(string username)
        {
            dataGridViewReport.Rows.Clear();
            List<Report> list = new ReportDAO().SelectByMonth(username);
            foreach (Report rep in list)
            {
                dataGridViewReport.Rows.Add(rep.ReportID, rep.Title, rep.Content, rep.Time, rep.Username);
            }
        }

        private void textBoxUserReport_TextChanged(object sender, EventArgs e)
        {
            string user = textBoxUserReport.Text;
            if (user.Length == 7)
            {
                OpenTabReport(user);
            }
            else
            {
                OpenTabReport("");
            }
        }

        private void dataGridViewReport_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridViewReport.Columns["detail"].Index && e.RowIndex >= 0)
            {
                DataGridViewCell cell = dataGridViewReport.Rows[e.RowIndex].Cells[0];
                int reportID = int.Parse(cell.Value.ToString());
                ReportForm form = new ReportForm(reportID);
                form.Show();
            }
        }

        // Profile tab
        public void OpenTabProfile()
        {
            Person per = new PersonDAO().SelectByUsername(username);
            Employee emp = new EmployeeDAO().SelectByUsername(username);
            this.SSN = per.SSN;

            pictureBoxAvatarPro.ImageLocation = emp.Image;
            textBoxNamePro.Text = per.Name;
            textBoxGenderPro.Text = (per.Gender) ? "Female" : "Male";
            dateTimePickerPro.Value = per.Dob;
            textBoxPhonePro.Text = per.Phone;
            textBoxAddressPro.Text = per.Address;
            textBoxPositionPro.Text = "Manager";
            textBoxUsernamePro.Text = username;
        }

        private void buttonUpdateProfile_Click(object sender, EventArgs e)
        {
            string phone = textBoxPhonePro.Text;

            string msg = new ValidUtil().ValidPhone(phone);
            if (msg == null)
            {
                new PersonDAO().ChangePhone(phone, SSN);
                MessageBox.Show("Update successfully.", "Success",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string ValidPassword()
        {
            string oldPass = textBoxOldPass.Text;
            oldPass = new EncodeUtil().EncryptPassword(oldPass);
            Employee emp = new Employee(username, oldPass);

            string msg = null;
            if (new EmployeeDAO().CheckLogin(emp))
            {
                string newPass = textBoxNewPass.Text;
                string rePass = textBoxRePass.Text;
                if (!newPass.Equals(rePass))
                {
                    msg = "Re-password doesn't match with new password.";
                }
            }
            else
            {
                msg = "Old password is incorrect.";
            }

            return msg;
        }

        private void buttonChangePass_Click(object sender, EventArgs e)
        {
            string msg = ValidPassword();
            if (msg == null)
            {
                string newPass = textBoxNewPass.Text;
                string password = new EncodeUtil().EncryptPassword(newPass);
                new EmployeeDAO().ChangePassword(new Employee(username, password));
                MessageBox.Show("Change password successfully.",
                    "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            ClearPassword();
        }

        private void ClearPassword()
        {
            textBoxOldPass.Text = string.Empty;
            textBoxNewPass.Text = string.Empty;
            textBoxRePass.Text = string.Empty;
        }
    }
}
