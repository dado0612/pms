﻿using PMS.dao;
using PMS.model;
using PMS.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebEye.Controls.WinForms.WebCameraControl;

namespace PMS.form
{
    public partial class Security : Form
    {
        private int parkID;
        private string username;
        private int cntANPR;
        private int cntQR;
        private string cardID;

        public Security()
        {
            InitializeComponent();
        }

        public Security(string username)
        {
            parkID = new Random().Next(4) + 1;
            this.username = username;

            InitializeComponent();
            LoadPanel0();
            LoadPanel1();
            LoadPanel2();
        }

        // panel[0][0]
        public void LoadPanel0()
        {
            Parking park = new ParkingDAO().SelectByID(parkID);
            textBoxUsername.Text = username;
            textBoxParking.Text = park.ParkName;
            textBoxMax.Text = "50";
            textBoxFree.Text = park.MaxPlace.ToString();
        }

        // panel[1][0]
        public void LoadPanel1()
        {
            WebCameraId id = new LibUtil().GetCamera(webcam);
            webcam.StartCapture(id);
        }

        // panel[2][0]
        public void LoadPanel2()
        {
            List<Track> list = new TrackDAO().SelectTop(parkID);
            foreach (Track track in list)
            {
                string status = (track.Status == (int)STATUS.IN) ? "CHECK IN" : "CHECK OUT";
                string time = string.Format("{0:HH:mm:ss dd/MM}", track.Time);
                dataGridView1.Rows.Add(track.PlateNumber, status, time, track.Username);
            }
        }

        public void UpdateButton(bool status)
        {
            buttonANPR.Enabled = status;
            buttonQR.Enabled = !status;
        }

        // ANPR
        private void buttonANPR_Click(object sender, EventArgs e)
        {
            string plate = null;
            while (plate == null)
            {
                string path = ConstUtil.TMP_IMAGE;
                path += "anpr" + ++cntANPR + ".bmp";
                webcam.GetCurrentImage().Save(path);
                pictureBox1.ImageLocation = path;
                plate = new LibUtil().ReadANPR(path);
            }

            LoadANPR(plate);
            UpdateButton(false);
        }

        private void LoadANPR(string plate)
        {
            List<Track> list = new TrackDAO().SelectByPlateNumber(plate);
            int status = (list.Count != 0) ? (1 - list.ElementAt(0).Status) : (int)STATUS.IN;

            textBoxPlateANPR.Text = plate;
            textBoxStatus.Text = (status == (int)STATUS.IN) ? "CHECK IN" : "CHECK OUT";
            textBoxTime.Text = DateTime.Now.ToString();
        }

        // QR
        private void buttonQR_Click(object sender, EventArgs e)
        {
            cardID = null;
            while (cardID == null)
            {
                string path = ConstUtil.TMP_IMAGE;
                path += "qr" + ++cntQR + ".bmp";
                webcam.GetCurrentImage().Save(path);
                cardID = new LibUtil().ReadQR(path);
            }
            LoadQR();
        }

        private bool CheckValidCard(Card card)
        {
            string plate = card.PlateNumber;
            if (card.ExpireDate < DateTime.Now || plate.Equals(""))
            {
                return true;
            }
            else
            {
                return textBoxPlateANPR.Text.Equals(plate);
            }
        }

        private void LoadQR()
        {
            Card card = new CardDAO().SelectByID(cardID);
            if (CheckValidCard(card))
            {
                textBoxSerial.Text = card.SerialNumber.ToString();
                DateTime expire = card.ExpireDate;

                if (expire < DateTime.Now)
                {
                    textBoxExpire.Text = "EXPIRE";
                }
                else
                {
                    textBoxPlateQR.Text = card.PlateNumber;
                    textBoxExpire.Text = expire.ToString();
                }
            }
            else
            {
                MessageBox.Show("This card doesn't match with plate number",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Add data
        private Track CreateTrack(long trackID)
        {
            int status = textBoxStatus.Text.EndsWith("IN") ? (int)STATUS.IN : (int)STATUS.OUT;
            DateTime time = DateTime.Parse(textBoxTime.Text);
            string image = trackID + ".jpg";
            return new Track(textBoxPlateANPR.Text, status, time, image, parkID, username);
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (textBoxSerial.Text.Equals(""))
            {
                MessageBox.Show("Please insert a valid card before",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                long trackID = new TrackDAO().GetMaxTrackID();
                Track track = CreateTrack(trackID);
                new TrackDAO().AddTrack(track);

                string status = (track.Status == (int)STATUS.IN) ? "CHECK IN" : "CHECK OUT";
                string time = string.Format("{0:HH:mm:ss dd/MM}", track.Time);
                dataGridView1.Rows.Insert(0, track.PlateNumber, status, time, track.Username);
                dataGridView1.Rows.RemoveAt(18);

                DateTime expire = DateTime.Parse(textBoxTime.Text);
                new CardDAO().UpdateCard(new Card(cardID, textBoxPlateANPR.Text, expire));

                string src = ConstUtil.TMP_IMAGE + "anpr" + cntANPR + ".bmp"; ;
                string dst = ConstUtil.TRACK_IMAGE + trackID + ".jpg";
                File.Move(src, dst);
                ClearData(track.Status);
            }
        }

        private void ClearData(int status)
        {
            textBoxPlateANPR.Text = string.Empty;
            textBoxStatus.Text = string.Empty;
            textBoxTime.Text = string.Empty;

            textBoxSerial.Text = string.Empty;
            textBoxPlateQR.Text = string.Empty;
            textBoxExpire.Text = string.Empty;

            UpdateButton(true);
            pictureBox1.ImageLocation = null;

            int place = int.Parse(textBoxFree.Text);
            place += (status == (int)STATUS.IN) ? -1 : 1;
            textBoxFree.Text = place.ToString();
        }
    }
}
