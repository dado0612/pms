﻿using WebEye.Controls.WinForms.WebCameraControl;

namespace PMS.form
{
    partial class Staff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageCustomer = new System.Windows.Forms.TabPage();
            this.groupBoxCustomer = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButtonFemale = new System.Windows.Forms.RadioButton();
            this.radioButtonMale = new System.Windows.Forms.RadioButton();
            this.buttonDone = new System.Windows.Forms.Button();
            this.dataGridViewPlate = new System.Windows.Forms.DataGridView();
            this.plateNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.delete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.textBoxNameCus = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.dateTimePickerCus = new System.Windows.Forms.DateTimePicker();
            this.textBoxAddressCus = new System.Windows.Forms.TextBox();
            this.textBoxPhoneCus = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxSSN = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tabPageCard = new System.Windows.Forms.TabPage();
            this.comboBoxRenew = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.buttonUpdateCard = new System.Windows.Forms.Button();
            this.textBoxExpire = new System.Windows.Forms.TextBox();
            this.textBoxSerial = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxPlateQR = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonQR = new System.Windows.Forms.Button();
            this.webcam = new WebEye.Controls.WinForms.WebCameraControl.WebCameraControl();
            this.tabPageReport = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBoxIn = new System.Windows.Forms.PictureBox();
            this.pictureBoxOut = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBoxContent = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxPlate = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tabPageProfile = new System.Windows.Forms.TabPage();
            this.textBoxGenderPro = new System.Windows.Forms.TextBox();
            this.buttonUpdateProfile = new System.Windows.Forms.Button();
            this.buttonChangePass = new System.Windows.Forms.Button();
            this.textBoxRePass = new System.Windows.Forms.TextBox();
            this.textBoxNewPass = new System.Windows.Forms.TextBox();
            this.textBoxOldPass = new System.Windows.Forms.TextBox();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimePickerPro = new System.Windows.Forms.DateTimePicker();
            this.textBoxPosition = new System.Windows.Forms.TextBox();
            this.textBoxAddressPro = new System.Windows.Forms.TextBox();
            this.textBoxPhonePro = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNamePro = new System.Windows.Forms.TextBox();
            this.pictureBoxAvatar = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tabPageCustomer.SuspendLayout();
            this.groupBoxCustomer.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlate)).BeginInit();
            this.tabPageCard.SuspendLayout();
            this.tabPageReport.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOut)).BeginInit();
            this.panel2.SuspendLayout();
            this.tabPageProfile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAvatar)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageCustomer);
            this.tabControl1.Controls.Add(this.tabPageCard);
            this.tabControl1.Controls.Add(this.tabPageReport);
            this.tabControl1.Controls.Add(this.tabPageProfile);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(982, 453);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPageCustomer
            // 
            this.tabPageCustomer.Controls.Add(this.groupBoxCustomer);
            this.tabPageCustomer.Controls.Add(this.textBoxSSN);
            this.tabPageCustomer.Controls.Add(this.label14);
            this.tabPageCustomer.Location = new System.Drawing.Point(4, 26);
            this.tabPageCustomer.Name = "tabPageCustomer";
            this.tabPageCustomer.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCustomer.Size = new System.Drawing.Size(974, 423);
            this.tabPageCustomer.TabIndex = 0;
            this.tabPageCustomer.Text = "Customer";
            this.tabPageCustomer.UseVisualStyleBackColor = true;
            // 
            // groupBoxCustomer
            // 
            this.groupBoxCustomer.Controls.Add(this.panel1);
            this.groupBoxCustomer.Controls.Add(this.buttonDone);
            this.groupBoxCustomer.Controls.Add(this.dataGridViewPlate);
            this.groupBoxCustomer.Controls.Add(this.textBoxNameCus);
            this.groupBoxCustomer.Controls.Add(this.label19);
            this.groupBoxCustomer.Controls.Add(this.dateTimePickerCus);
            this.groupBoxCustomer.Controls.Add(this.textBoxAddressCus);
            this.groupBoxCustomer.Controls.Add(this.textBoxPhoneCus);
            this.groupBoxCustomer.Controls.Add(this.label15);
            this.groupBoxCustomer.Controls.Add(this.label16);
            this.groupBoxCustomer.Controls.Add(this.label17);
            this.groupBoxCustomer.Controls.Add(this.label18);
            this.groupBoxCustomer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxCustomer.Location = new System.Drawing.Point(3, 70);
            this.groupBoxCustomer.Name = "groupBoxCustomer";
            this.groupBoxCustomer.Size = new System.Drawing.Size(968, 350);
            this.groupBoxCustomer.TabIndex = 2;
            this.groupBoxCustomer.TabStop = false;
            this.groupBoxCustomer.Text = "Add Customer";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButtonFemale);
            this.panel1.Controls.Add(this.radioButtonMale);
            this.panel1.Location = new System.Drawing.Point(200, 97);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 30);
            this.panel1.TabIndex = 36;
            // 
            // radioButtonFemale
            // 
            this.radioButtonFemale.AutoSize = true;
            this.radioButtonFemale.Location = new System.Drawing.Point(101, 4);
            this.radioButtonFemale.Name = "radioButtonFemale";
            this.radioButtonFemale.Size = new System.Drawing.Size(72, 21);
            this.radioButtonFemale.TabIndex = 1;
            this.radioButtonFemale.TabStop = true;
            this.radioButtonFemale.Text = "Female";
            this.radioButtonFemale.UseVisualStyleBackColor = true;
            // 
            // radioButtonMale
            // 
            this.radioButtonMale.AutoSize = true;
            this.radioButtonMale.Checked = true;
            this.radioButtonMale.Location = new System.Drawing.Point(18, 4);
            this.radioButtonMale.Name = "radioButtonMale";
            this.radioButtonMale.Size = new System.Drawing.Size(56, 21);
            this.radioButtonMale.TabIndex = 0;
            this.radioButtonMale.TabStop = true;
            this.radioButtonMale.Text = "Male";
            this.radioButtonMale.UseVisualStyleBackColor = true;
            // 
            // buttonDone
            // 
            this.buttonDone.Location = new System.Drawing.Point(700, 288);
            this.buttonDone.Name = "buttonDone";
            this.buttonDone.Size = new System.Drawing.Size(150, 27);
            this.buttonDone.TabIndex = 35;
            this.buttonDone.Text = "Done";
            this.buttonDone.UseVisualStyleBackColor = true;
            this.buttonDone.Click += new System.EventHandler(this.buttonDone_Click);
            // 
            // dataGridViewPlate
            // 
            this.dataGridViewPlate.AllowUserToDeleteRows = false;
            this.dataGridViewPlate.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPlate.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewPlate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPlate.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.plateNumber,
            this.delete});
            this.dataGridViewPlate.Location = new System.Drawing.Point(554, 97);
            this.dataGridViewPlate.Name = "dataGridViewPlate";
            this.dataGridViewPlate.RowHeadersVisible = false;
            this.dataGridViewPlate.RowTemplate.Height = 24;
            this.dataGridViewPlate.Size = new System.Drawing.Size(296, 143);
            this.dataGridViewPlate.TabIndex = 34;
            this.dataGridViewPlate.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPlate_CellContentClick);
            this.dataGridViewPlate.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPlate_CellEndEdit);
            // 
            // plateNumber
            // 
            this.plateNumber.FillWeight = 131.9797F;
            this.plateNumber.HeaderText = "Plate Number";
            this.plateNumber.Name = "plateNumber";
            // 
            // delete
            // 
            this.delete.FillWeight = 68.02031F;
            this.delete.HeaderText = "Delete";
            this.delete.Name = "delete";
            this.delete.Text = "Delete";
            this.delete.UseColumnTextForLinkValue = true;
            // 
            // textBoxNameCus
            // 
            this.textBoxNameCus.Location = new System.Drawing.Point(200, 37);
            this.textBoxNameCus.Name = "textBoxNameCus";
            this.textBoxNameCus.Size = new System.Drawing.Size(200, 23);
            this.textBoxNameCus.TabIndex = 32;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(100, 40);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(45, 17);
            this.label19.TabIndex = 31;
            this.label19.Text = "Name";
            // 
            // dateTimePickerCus
            // 
            this.dateTimePickerCus.CustomFormat = "ddd, dd/MM/yyyy";
            this.dateTimePickerCus.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerCus.Location = new System.Drawing.Point(200, 157);
            this.dateTimePickerCus.Name = "dateTimePickerCus";
            this.dateTimePickerCus.Size = new System.Drawing.Size(200, 23);
            this.dateTimePickerCus.TabIndex = 29;
            // 
            // textBoxAddressCus
            // 
            this.textBoxAddressCus.Location = new System.Drawing.Point(200, 217);
            this.textBoxAddressCus.Multiline = true;
            this.textBoxAddressCus.Name = "textBoxAddressCus";
            this.textBoxAddressCus.Size = new System.Drawing.Size(200, 100);
            this.textBoxAddressCus.TabIndex = 28;
            // 
            // textBoxPhoneCus
            // 
            this.textBoxPhoneCus.Location = new System.Drawing.Point(650, 37);
            this.textBoxPhoneCus.Name = "textBoxPhoneCus";
            this.textBoxPhoneCus.Size = new System.Drawing.Size(200, 23);
            this.textBoxPhoneCus.TabIndex = 27;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(100, 220);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 17);
            this.label15.TabIndex = 26;
            this.label15.Text = "Address";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(550, 40);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 17);
            this.label16.TabIndex = 25;
            this.label16.Text = "Phone";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(100, 160);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 17);
            this.label17.TabIndex = 24;
            this.label17.Text = "DOB";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(100, 100);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 17);
            this.label18.TabIndex = 23;
            this.label18.Text = "Gender";
            // 
            // textBoxSSN
            // 
            this.textBoxSSN.Location = new System.Drawing.Point(250, 22);
            this.textBoxSSN.Name = "textBoxSSN";
            this.textBoxSSN.Size = new System.Drawing.Size(200, 23);
            this.textBoxSSN.TabIndex = 1;
            this.textBoxSSN.TextChanged += new System.EventHandler(this.textBoxSSN_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(100, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(104, 17);
            this.label14.TabIndex = 0;
            this.label14.Text = "Search by SSN";
            // 
            // tabPageCard
            // 
            this.tabPageCard.Controls.Add(this.comboBoxRenew);
            this.tabPageCard.Controls.Add(this.label13);
            this.tabPageCard.Controls.Add(this.buttonUpdateCard);
            this.tabPageCard.Controls.Add(this.textBoxExpire);
            this.tabPageCard.Controls.Add(this.textBoxSerial);
            this.tabPageCard.Controls.Add(this.label10);
            this.tabPageCard.Controls.Add(this.label11);
            this.tabPageCard.Controls.Add(this.textBoxPlateQR);
            this.tabPageCard.Controls.Add(this.label12);
            this.tabPageCard.Controls.Add(this.buttonQR);
            this.tabPageCard.Controls.Add(this.webcam);
            this.tabPageCard.Location = new System.Drawing.Point(4, 26);
            this.tabPageCard.Name = "tabPageCard";
            this.tabPageCard.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCard.Size = new System.Drawing.Size(974, 423);
            this.tabPageCard.TabIndex = 1;
            this.tabPageCard.Text = "Card";
            this.tabPageCard.UseVisualStyleBackColor = true;
            // 
            // comboBoxRenew
            // 
            this.comboBoxRenew.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRenew.FormattingEnabled = true;
            this.comboBoxRenew.Items.AddRange(new object[] {
            "1 month",
            "2 months",
            "3 months"});
            this.comboBoxRenew.Location = new System.Drawing.Point(600, 277);
            this.comboBoxRenew.Name = "comboBoxRenew";
            this.comboBoxRenew.Size = new System.Drawing.Size(200, 25);
            this.comboBoxRenew.TabIndex = 26;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(450, 280);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 17);
            this.label13.TabIndex = 25;
            this.label13.Text = "Renew Card";
            // 
            // buttonUpdateCard
            // 
            this.buttonUpdateCard.Enabled = false;
            this.buttonUpdateCard.Location = new System.Drawing.Point(650, 355);
            this.buttonUpdateCard.Name = "buttonUpdateCard";
            this.buttonUpdateCard.Size = new System.Drawing.Size(150, 27);
            this.buttonUpdateCard.TabIndex = 24;
            this.buttonUpdateCard.Text = "Update Card";
            this.buttonUpdateCard.UseVisualStyleBackColor = true;
            this.buttonUpdateCard.Click += new System.EventHandler(this.buttonUpdateCard_Click);
            // 
            // textBoxExpire
            // 
            this.textBoxExpire.Enabled = false;
            this.textBoxExpire.Location = new System.Drawing.Point(600, 217);
            this.textBoxExpire.Name = "textBoxExpire";
            this.textBoxExpire.Size = new System.Drawing.Size(200, 23);
            this.textBoxExpire.TabIndex = 23;
            // 
            // textBoxSerial
            // 
            this.textBoxSerial.Enabled = false;
            this.textBoxSerial.Location = new System.Drawing.Point(600, 157);
            this.textBoxSerial.Name = "textBoxSerial";
            this.textBoxSerial.Size = new System.Drawing.Size(200, 23);
            this.textBoxSerial.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(450, 220);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 17);
            this.label10.TabIndex = 21;
            this.label10.Text = "Expire Date";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(450, 160);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 17);
            this.label11.TabIndex = 20;
            this.label11.Text = "Serial Number";
            // 
            // textBoxPlateQR
            // 
            this.textBoxPlateQR.Location = new System.Drawing.Point(600, 97);
            this.textBoxPlateQR.Name = "textBoxPlateQR";
            this.textBoxPlateQR.Size = new System.Drawing.Size(200, 23);
            this.textBoxPlateQR.TabIndex = 19;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(450, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 17);
            this.label12.TabIndex = 18;
            this.label12.Text = "Plate Number";
            // 
            // buttonQR
            // 
            this.buttonQR.Location = new System.Drawing.Point(138, 355);
            this.buttonQR.Name = "buttonQR";
            this.buttonQR.Size = new System.Drawing.Size(100, 27);
            this.buttonQR.TabIndex = 2;
            this.buttonQR.Text = "Scan QR";
            this.buttonQR.UseVisualStyleBackColor = true;
            this.buttonQR.Click += new System.EventHandler(this.buttonQR_Click);
            // 
            // webcam
            // 
            this.webcam.Location = new System.Drawing.Point(25, 25);
            this.webcam.Margin = new System.Windows.Forms.Padding(5);
            this.webcam.Name = "webcam";
            this.webcam.Size = new System.Drawing.Size(333, 301);
            this.webcam.TabIndex = 1;
            // 
            // tabPageReport
            // 
            this.tabPageReport.Controls.Add(this.tableLayoutPanel1);
            this.tabPageReport.Location = new System.Drawing.Point(4, 26);
            this.tabPageReport.Name = "tabPageReport";
            this.tabPageReport.Size = new System.Drawing.Size(974, 423);
            this.tabPageReport.TabIndex = 2;
            this.tabPageReport.Text = "Report";
            this.tabPageReport.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBoxIn, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureBoxOut, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 423F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(974, 423);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pictureBoxIn
            // 
            this.pictureBoxIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxIn.Location = new System.Drawing.Point(392, 3);
            this.pictureBoxIn.Name = "pictureBoxIn";
            this.pictureBoxIn.Size = new System.Drawing.Size(286, 417);
            this.pictureBoxIn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxIn.TabIndex = 0;
            this.pictureBoxIn.TabStop = false;
            // 
            // pictureBoxOut
            // 
            this.pictureBoxOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxOut.Location = new System.Drawing.Point(684, 3);
            this.pictureBoxOut.Name = "pictureBoxOut";
            this.pictureBoxOut.Size = new System.Drawing.Size(287, 417);
            this.pictureBoxOut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxOut.TabIndex = 1;
            this.pictureBoxOut.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonAdd);
            this.panel2.Controls.Add(this.textBoxTitle);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.textBoxContent);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.textBoxPlate);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(383, 417);
            this.panel2.TabIndex = 2;
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(225, 380);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(100, 23);
            this.buttonAdd.TabIndex = 8;
            this.buttonAdd.Text = "Add Report";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Location = new System.Drawing.Point(25, 125);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(300, 23);
            this.textBoxTitle.TabIndex = 7;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(25, 100);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 17);
            this.label22.TabIndex = 6;
            this.label22.Text = "Title";
            // 
            // textBoxContent
            // 
            this.textBoxContent.Location = new System.Drawing.Point(25, 200);
            this.textBoxContent.Multiline = true;
            this.textBoxContent.Name = "textBoxContent";
            this.textBoxContent.Size = new System.Drawing.Size(300, 153);
            this.textBoxContent.TabIndex = 5;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(25, 175);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(57, 17);
            this.label21.TabIndex = 4;
            this.label21.Text = "Content";
            // 
            // textBoxPlate
            // 
            this.textBoxPlate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxPlate.Location = new System.Drawing.Point(25, 50);
            this.textBoxPlate.Name = "textBoxPlate";
            this.textBoxPlate.Size = new System.Drawing.Size(200, 23);
            this.textBoxPlate.TabIndex = 3;
            this.textBoxPlate.TextChanged += new System.EventHandler(this.textBoxPlate_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(25, 25);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(202, 17);
            this.label20.TabIndex = 2;
            this.label20.Text = "Search Track by Plate Number";
            // 
            // tabPageProfile
            // 
            this.tabPageProfile.Controls.Add(this.textBoxGenderPro);
            this.tabPageProfile.Controls.Add(this.buttonUpdateProfile);
            this.tabPageProfile.Controls.Add(this.buttonChangePass);
            this.tabPageProfile.Controls.Add(this.textBoxRePass);
            this.tabPageProfile.Controls.Add(this.textBoxNewPass);
            this.tabPageProfile.Controls.Add(this.textBoxOldPass);
            this.tabPageProfile.Controls.Add(this.textBoxUsername);
            this.tabPageProfile.Controls.Add(this.label6);
            this.tabPageProfile.Controls.Add(this.label7);
            this.tabPageProfile.Controls.Add(this.label8);
            this.tabPageProfile.Controls.Add(this.label9);
            this.tabPageProfile.Controls.Add(this.dateTimePickerPro);
            this.tabPageProfile.Controls.Add(this.textBoxPosition);
            this.tabPageProfile.Controls.Add(this.textBoxAddressPro);
            this.tabPageProfile.Controls.Add(this.textBoxPhonePro);
            this.tabPageProfile.Controls.Add(this.label5);
            this.tabPageProfile.Controls.Add(this.label4);
            this.tabPageProfile.Controls.Add(this.label3);
            this.tabPageProfile.Controls.Add(this.label2);
            this.tabPageProfile.Controls.Add(this.label1);
            this.tabPageProfile.Controls.Add(this.textBoxNamePro);
            this.tabPageProfile.Controls.Add(this.pictureBoxAvatar);
            this.tabPageProfile.Location = new System.Drawing.Point(4, 26);
            this.tabPageProfile.Name = "tabPageProfile";
            this.tabPageProfile.Size = new System.Drawing.Size(974, 423);
            this.tabPageProfile.TabIndex = 3;
            this.tabPageProfile.Text = "Profile";
            this.tabPageProfile.UseVisualStyleBackColor = true;
            // 
            // textBoxGenderPro
            // 
            this.textBoxGenderPro.Enabled = false;
            this.textBoxGenderPro.Location = new System.Drawing.Point(350, 47);
            this.textBoxGenderPro.Name = "textBoxGenderPro";
            this.textBoxGenderPro.Size = new System.Drawing.Size(200, 23);
            this.textBoxGenderPro.TabIndex = 22;
            // 
            // buttonUpdateProfile
            // 
            this.buttonUpdateProfile.Location = new System.Drawing.Point(350, 347);
            this.buttonUpdateProfile.Name = "buttonUpdateProfile";
            this.buttonUpdateProfile.Size = new System.Drawing.Size(200, 27);
            this.buttonUpdateProfile.TabIndex = 21;
            this.buttonUpdateProfile.Text = "Update Profile";
            this.buttonUpdateProfile.UseVisualStyleBackColor = true;
            this.buttonUpdateProfile.Click += new System.EventHandler(this.buttonUpdateProfile_Click);
            // 
            // buttonChangePass
            // 
            this.buttonChangePass.Location = new System.Drawing.Point(750, 347);
            this.buttonChangePass.Name = "buttonChangePass";
            this.buttonChangePass.Size = new System.Drawing.Size(200, 27);
            this.buttonChangePass.TabIndex = 20;
            this.buttonChangePass.Text = "Change Password";
            this.buttonChangePass.UseVisualStyleBackColor = true;
            this.buttonChangePass.Click += new System.EventHandler(this.buttonChangePass_Click);
            // 
            // textBoxRePass
            // 
            this.textBoxRePass.Location = new System.Drawing.Point(750, 287);
            this.textBoxRePass.Name = "textBoxRePass";
            this.textBoxRePass.PasswordChar = '*';
            this.textBoxRePass.Size = new System.Drawing.Size(200, 23);
            this.textBoxRePass.TabIndex = 19;
            // 
            // textBoxNewPass
            // 
            this.textBoxNewPass.Location = new System.Drawing.Point(750, 227);
            this.textBoxNewPass.Name = "textBoxNewPass";
            this.textBoxNewPass.PasswordChar = '*';
            this.textBoxNewPass.Size = new System.Drawing.Size(200, 23);
            this.textBoxNewPass.TabIndex = 18;
            // 
            // textBoxOldPass
            // 
            this.textBoxOldPass.Location = new System.Drawing.Point(750, 167);
            this.textBoxOldPass.Name = "textBoxOldPass";
            this.textBoxOldPass.PasswordChar = '*';
            this.textBoxOldPass.Size = new System.Drawing.Size(200, 23);
            this.textBoxOldPass.TabIndex = 17;
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Enabled = false;
            this.textBoxUsername.Location = new System.Drawing.Point(750, 107);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(200, 23);
            this.textBoxUsername.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(600, 290);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "Re-password";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(600, 230);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "New password";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(600, 170);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 17);
            this.label8.TabIndex = 13;
            this.label8.Text = "Old password";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(600, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 17);
            this.label9.TabIndex = 12;
            this.label9.Text = "Username";
            // 
            // dateTimePickerPro
            // 
            this.dateTimePickerPro.CustomFormat = "ddd, dd/MM/yyyy";
            this.dateTimePickerPro.Enabled = false;
            this.dateTimePickerPro.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerPro.Location = new System.Drawing.Point(350, 107);
            this.dateTimePickerPro.Name = "dateTimePickerPro";
            this.dateTimePickerPro.Size = new System.Drawing.Size(200, 23);
            this.dateTimePickerPro.TabIndex = 11;
            // 
            // textBoxPosition
            // 
            this.textBoxPosition.Enabled = false;
            this.textBoxPosition.Location = new System.Drawing.Point(750, 47);
            this.textBoxPosition.Name = "textBoxPosition";
            this.textBoxPosition.Size = new System.Drawing.Size(200, 23);
            this.textBoxPosition.TabIndex = 9;
            // 
            // textBoxAddressPro
            // 
            this.textBoxAddressPro.Enabled = false;
            this.textBoxAddressPro.Location = new System.Drawing.Point(350, 227);
            this.textBoxAddressPro.Multiline = true;
            this.textBoxAddressPro.Name = "textBoxAddressPro";
            this.textBoxAddressPro.Size = new System.Drawing.Size(200, 80);
            this.textBoxAddressPro.TabIndex = 8;
            // 
            // textBoxPhonePro
            // 
            this.textBoxPhonePro.Location = new System.Drawing.Point(350, 167);
            this.textBoxPhonePro.Name = "textBoxPhonePro";
            this.textBoxPhonePro.Size = new System.Drawing.Size(200, 23);
            this.textBoxPhonePro.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(600, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Position";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(250, 230);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(250, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Phone";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(250, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "DOB";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(250, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Gender";
            // 
            // textBoxNamePro
            // 
            this.textBoxNamePro.Enabled = false;
            this.textBoxNamePro.Location = new System.Drawing.Point(25, 347);
            this.textBoxNamePro.Name = "textBoxNamePro";
            this.textBoxNamePro.Size = new System.Drawing.Size(200, 23);
            this.textBoxNamePro.TabIndex = 1;
            // 
            // pictureBoxAvatar
            // 
            this.pictureBoxAvatar.Location = new System.Drawing.Point(25, 50);
            this.pictureBoxAvatar.Name = "pictureBoxAvatar";
            this.pictureBoxAvatar.Size = new System.Drawing.Size(200, 250);
            this.pictureBoxAvatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxAvatar.TabIndex = 0;
            this.pictureBoxAvatar.TabStop = false;
            // 
            // Staff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 453);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "Staff";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Staff";
            this.tabControl1.ResumeLayout(false);
            this.tabPageCustomer.ResumeLayout(false);
            this.tabPageCustomer.PerformLayout();
            this.groupBoxCustomer.ResumeLayout(false);
            this.groupBoxCustomer.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlate)).EndInit();
            this.tabPageCard.ResumeLayout(false);
            this.tabPageCard.PerformLayout();
            this.tabPageReport.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOut)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPageProfile.ResumeLayout(false);
            this.tabPageProfile.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAvatar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private WebCameraControl webcam;
        private System.Windows.Forms.TabPage tabPageCustomer;
        private System.Windows.Forms.TabPage tabPageCard;
        private System.Windows.Forms.TabPage tabPageReport;
        private System.Windows.Forms.TabPage tabPageProfile;
        private System.Windows.Forms.Button buttonUpdateProfile;
        private System.Windows.Forms.Button buttonChangePass;
        private System.Windows.Forms.TextBox textBoxRePass;
        private System.Windows.Forms.TextBox textBoxNewPass;
        private System.Windows.Forms.TextBox textBoxOldPass;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTimePickerPro;
        private System.Windows.Forms.TextBox textBoxPosition;
        private System.Windows.Forms.TextBox textBoxAddressPro;
        private System.Windows.Forms.TextBox textBoxPhonePro;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNamePro;
        private System.Windows.Forms.PictureBox pictureBoxAvatar;
        private System.Windows.Forms.TextBox textBoxGenderPro;
        private System.Windows.Forms.Button buttonQR;
        private System.Windows.Forms.Button buttonUpdateCard;
        private System.Windows.Forms.TextBox textBoxExpire;
        private System.Windows.Forms.TextBox textBoxSerial;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxPlateQR;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxRenew;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBoxCustomer;
        private System.Windows.Forms.TextBox textBoxSSN;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView dataGridViewPlate;
        private System.Windows.Forms.TextBox textBoxNameCus;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DateTimePicker dateTimePickerCus;
        private System.Windows.Forms.TextBox textBoxAddressCus;
        private System.Windows.Forms.TextBox textBoxPhoneCus;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button buttonDone;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButtonFemale;
        private System.Windows.Forms.RadioButton radioButtonMale;
        private System.Windows.Forms.DataGridViewTextBoxColumn plateNumber;
        private System.Windows.Forms.DataGridViewLinkColumn delete;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBoxIn;
        private System.Windows.Forms.PictureBox pictureBoxOut;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBoxContent;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxPlate;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button buttonAdd;
    }
}