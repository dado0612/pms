﻿using PMS.dao;
using PMS.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PMS.form
{
    public partial class ReportForm : Form
    {
        public ReportForm()
        {
            InitializeComponent();
        }

        public ReportForm(int reportID)
        {
            InitializeComponent();

            Report rep = new ReportDAO().SelectByID(reportID);
            textBoxReport.Text = rep.ReportID.ToString();
            textBoxTrack.Text = rep.TrackID.ToString();
            dateTimePicker1.Value = rep.Time;
            textBoxTitle.Text = rep.Title;
            textBoxContent.Text = rep.Content;
        }
    }
}
