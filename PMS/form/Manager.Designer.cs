﻿namespace PMS.form
{
    partial class Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageStatistic = new System.Windows.Forms.TabPage();
            this.tabPageEmployee = new System.Windows.Forms.TabPage();
            this.groupBoxEmployee = new System.Windows.Forms.GroupBox();
            this.buttonImage = new System.Windows.Forms.Button();
            this.textBoxUsernameEmp = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxRole = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.pictureBoxAvatarEmp = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButtonFemale = new System.Windows.Forms.RadioButton();
            this.radioButtonMale = new System.Windows.Forms.RadioButton();
            this.buttonDone = new System.Windows.Forms.Button();
            this.textBoxNameEmp = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.dateTimePickerEmp = new System.Windows.Forms.DateTimePicker();
            this.textBoxAddressEmp = new System.Windows.Forms.TextBox();
            this.textBoxPhoneEmp = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxSSN = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPageReport = new System.Windows.Forms.TabPage();
            this.dataGridViewReport = new System.Windows.Forms.DataGridView();
            this.reportID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.user = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.detail = new System.Windows.Forms.DataGridViewLinkColumn();
            this.textBoxUserReport = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tabPageProfile = new System.Windows.Forms.TabPage();
            this.textBoxGenderPro = new System.Windows.Forms.TextBox();
            this.buttonUpdateProfile = new System.Windows.Forms.Button();
            this.buttonChangePass = new System.Windows.Forms.Button();
            this.textBoxRePass = new System.Windows.Forms.TextBox();
            this.textBoxNewPass = new System.Windows.Forms.TextBox();
            this.textBoxOldPass = new System.Windows.Forms.TextBox();
            this.textBoxUsernamePro = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimePickerPro = new System.Windows.Forms.DateTimePicker();
            this.textBoxPositionPro = new System.Windows.Forms.TextBox();
            this.textBoxAddressPro = new System.Windows.Forms.TextBox();
            this.textBoxPhonePro = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNamePro = new System.Windows.Forms.TextBox();
            this.pictureBoxAvatarPro = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.chartTrack = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartCard = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.comboBoxStatistic = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPageStatistic.SuspendLayout();
            this.tabPageEmployee.SuspendLayout();
            this.groupBoxEmployee.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAvatarEmp)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabPageReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).BeginInit();
            this.tabPageProfile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAvatarPro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartTrack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartCard)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageStatistic);
            this.tabControl1.Controls.Add(this.tabPageEmployee);
            this.tabControl1.Controls.Add(this.tabPageReport);
            this.tabControl1.Controls.Add(this.tabPageProfile);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(982, 453);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPageStatistic
            // 
            this.tabPageStatistic.Controls.Add(this.label21);
            this.tabPageStatistic.Controls.Add(this.label20);
            this.tabPageStatistic.Controls.Add(this.comboBoxStatistic);
            this.tabPageStatistic.Controls.Add(this.label13);
            this.tabPageStatistic.Controls.Add(this.chartCard);
            this.tabPageStatistic.Controls.Add(this.chartTrack);
            this.tabPageStatistic.Location = new System.Drawing.Point(4, 26);
            this.tabPageStatistic.Name = "tabPageStatistic";
            this.tabPageStatistic.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStatistic.Size = new System.Drawing.Size(974, 423);
            this.tabPageStatistic.TabIndex = 0;
            this.tabPageStatistic.Text = "Statistic";
            this.tabPageStatistic.UseVisualStyleBackColor = true;
            // 
            // tabPageEmployee
            // 
            this.tabPageEmployee.Controls.Add(this.groupBoxEmployee);
            this.tabPageEmployee.Controls.Add(this.textBoxSSN);
            this.tabPageEmployee.Controls.Add(this.label10);
            this.tabPageEmployee.Location = new System.Drawing.Point(4, 26);
            this.tabPageEmployee.Name = "tabPageEmployee";
            this.tabPageEmployee.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEmployee.Size = new System.Drawing.Size(974, 423);
            this.tabPageEmployee.TabIndex = 1;
            this.tabPageEmployee.Text = "Employee";
            this.tabPageEmployee.UseVisualStyleBackColor = true;
            // 
            // groupBoxEmployee
            // 
            this.groupBoxEmployee.Controls.Add(this.buttonImage);
            this.groupBoxEmployee.Controls.Add(this.textBoxUsernameEmp);
            this.groupBoxEmployee.Controls.Add(this.label11);
            this.groupBoxEmployee.Controls.Add(this.comboBoxRole);
            this.groupBoxEmployee.Controls.Add(this.label12);
            this.groupBoxEmployee.Controls.Add(this.pictureBoxAvatarEmp);
            this.groupBoxEmployee.Controls.Add(this.panel1);
            this.groupBoxEmployee.Controls.Add(this.buttonDone);
            this.groupBoxEmployee.Controls.Add(this.textBoxNameEmp);
            this.groupBoxEmployee.Controls.Add(this.label19);
            this.groupBoxEmployee.Controls.Add(this.dateTimePickerEmp);
            this.groupBoxEmployee.Controls.Add(this.textBoxAddressEmp);
            this.groupBoxEmployee.Controls.Add(this.textBoxPhoneEmp);
            this.groupBoxEmployee.Controls.Add(this.label15);
            this.groupBoxEmployee.Controls.Add(this.label16);
            this.groupBoxEmployee.Controls.Add(this.label17);
            this.groupBoxEmployee.Controls.Add(this.label18);
            this.groupBoxEmployee.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxEmployee.Location = new System.Drawing.Point(3, 70);
            this.groupBoxEmployee.Name = "groupBoxEmployee";
            this.groupBoxEmployee.Size = new System.Drawing.Size(968, 350);
            this.groupBoxEmployee.TabIndex = 5;
            this.groupBoxEmployee.TabStop = false;
            this.groupBoxEmployee.Text = "Add Employee";
            // 
            // buttonImage
            // 
            this.buttonImage.Location = new System.Drawing.Point(275, 275);
            this.buttonImage.Name = "buttonImage";
            this.buttonImage.Size = new System.Drawing.Size(100, 25);
            this.buttonImage.TabIndex = 40;
            this.buttonImage.Text = "Add Image";
            this.buttonImage.UseVisualStyleBackColor = true;
            this.buttonImage.Click += new System.EventHandler(this.buttonImage_Click);
            // 
            // textBoxUsernameEmp
            // 
            this.textBoxUsernameEmp.Enabled = false;
            this.textBoxUsernameEmp.Location = new System.Drawing.Point(700, 167);
            this.textBoxUsernameEmp.Name = "textBoxUsernameEmp";
            this.textBoxUsernameEmp.Size = new System.Drawing.Size(200, 23);
            this.textBoxUsernameEmp.TabIndex = 7;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(600, 170);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 17);
            this.label11.TabIndex = 6;
            this.label11.Text = "Username";
            // 
            // comboBoxRole
            // 
            this.comboBoxRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRole.FormattingEnabled = true;
            this.comboBoxRole.Location = new System.Drawing.Point(700, 227);
            this.comboBoxRole.Name = "comboBoxRole";
            this.comboBoxRole.Size = new System.Drawing.Size(200, 25);
            this.comboBoxRole.TabIndex = 39;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(600, 230);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 17);
            this.label12.TabIndex = 38;
            this.label12.Text = "Position";
            // 
            // pictureBoxAvatarEmp
            // 
            this.pictureBoxAvatarEmp.Location = new System.Drawing.Point(25, 50);
            this.pictureBoxAvatarEmp.Name = "pictureBoxAvatarEmp";
            this.pictureBoxAvatarEmp.Size = new System.Drawing.Size(200, 250);
            this.pictureBoxAvatarEmp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxAvatarEmp.TabIndex = 37;
            this.pictureBoxAvatarEmp.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButtonFemale);
            this.panel1.Controls.Add(this.radioButtonMale);
            this.panel1.Location = new System.Drawing.Point(350, 107);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 30);
            this.panel1.TabIndex = 36;
            // 
            // radioButtonFemale
            // 
            this.radioButtonFemale.AutoSize = true;
            this.radioButtonFemale.Location = new System.Drawing.Point(101, 4);
            this.radioButtonFemale.Name = "radioButtonFemale";
            this.radioButtonFemale.Size = new System.Drawing.Size(72, 21);
            this.radioButtonFemale.TabIndex = 1;
            this.radioButtonFemale.TabStop = true;
            this.radioButtonFemale.Text = "Female";
            this.radioButtonFemale.UseVisualStyleBackColor = true;
            // 
            // radioButtonMale
            // 
            this.radioButtonMale.AutoSize = true;
            this.radioButtonMale.Checked = true;
            this.radioButtonMale.Location = new System.Drawing.Point(18, 4);
            this.radioButtonMale.Name = "radioButtonMale";
            this.radioButtonMale.Size = new System.Drawing.Size(56, 21);
            this.radioButtonMale.TabIndex = 0;
            this.radioButtonMale.TabStop = true;
            this.radioButtonMale.Text = "Male";
            this.radioButtonMale.UseVisualStyleBackColor = true;
            // 
            // buttonDone
            // 
            this.buttonDone.Location = new System.Drawing.Point(750, 275);
            this.buttonDone.Name = "buttonDone";
            this.buttonDone.Size = new System.Drawing.Size(150, 25);
            this.buttonDone.TabIndex = 35;
            this.buttonDone.Text = "Add Employee";
            this.buttonDone.UseVisualStyleBackColor = true;
            this.buttonDone.Click += new System.EventHandler(this.buttonDone_Click);
            // 
            // textBoxNameEmp
            // 
            this.textBoxNameEmp.Location = new System.Drawing.Point(350, 47);
            this.textBoxNameEmp.Name = "textBoxNameEmp";
            this.textBoxNameEmp.Size = new System.Drawing.Size(200, 23);
            this.textBoxNameEmp.TabIndex = 32;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(275, 50);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(45, 17);
            this.label19.TabIndex = 31;
            this.label19.Text = "Name";
            // 
            // dateTimePickerEmp
            // 
            this.dateTimePickerEmp.CustomFormat = "ddd, dd/MM/yyyy";
            this.dateTimePickerEmp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerEmp.Location = new System.Drawing.Point(350, 167);
            this.dateTimePickerEmp.Name = "dateTimePickerEmp";
            this.dateTimePickerEmp.Size = new System.Drawing.Size(200, 23);
            this.dateTimePickerEmp.TabIndex = 29;
            // 
            // textBoxAddressEmp
            // 
            this.textBoxAddressEmp.Location = new System.Drawing.Point(700, 47);
            this.textBoxAddressEmp.Multiline = true;
            this.textBoxAddressEmp.Name = "textBoxAddressEmp";
            this.textBoxAddressEmp.Size = new System.Drawing.Size(200, 100);
            this.textBoxAddressEmp.TabIndex = 28;
            // 
            // textBoxPhoneEmp
            // 
            this.textBoxPhoneEmp.Location = new System.Drawing.Point(350, 227);
            this.textBoxPhoneEmp.Name = "textBoxPhoneEmp";
            this.textBoxPhoneEmp.Size = new System.Drawing.Size(200, 23);
            this.textBoxPhoneEmp.TabIndex = 27;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(600, 50);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 17);
            this.label15.TabIndex = 26;
            this.label15.Text = "Address";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(275, 230);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 17);
            this.label16.TabIndex = 25;
            this.label16.Text = "Phone";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(275, 170);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 17);
            this.label17.TabIndex = 24;
            this.label17.Text = "DOB";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(275, 110);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 17);
            this.label18.TabIndex = 23;
            this.label18.Text = "Gender";
            // 
            // textBoxSSN
            // 
            this.textBoxSSN.Location = new System.Drawing.Point(225, 22);
            this.textBoxSSN.Name = "textBoxSSN";
            this.textBoxSSN.Size = new System.Drawing.Size(200, 23);
            this.textBoxSSN.TabIndex = 4;
            this.textBoxSSN.TextChanged += new System.EventHandler(this.textBoxSSN_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(100, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 17);
            this.label10.TabIndex = 3;
            this.label10.Text = "Search by SSN";
            // 
            // tabPageReport
            // 
            this.tabPageReport.Controls.Add(this.dataGridViewReport);
            this.tabPageReport.Controls.Add(this.textBoxUserReport);
            this.tabPageReport.Controls.Add(this.label14);
            this.tabPageReport.Location = new System.Drawing.Point(4, 26);
            this.tabPageReport.Name = "tabPageReport";
            this.tabPageReport.Size = new System.Drawing.Size(974, 423);
            this.tabPageReport.TabIndex = 2;
            this.tabPageReport.Text = "Report";
            this.tabPageReport.UseVisualStyleBackColor = true;
            // 
            // dataGridViewReport
            // 
            this.dataGridViewReport.AllowUserToAddRows = false;
            this.dataGridViewReport.AllowUserToDeleteRows = false;
            this.dataGridViewReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewReport.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.reportID,
            this.title,
            this.content,
            this.time,
            this.user,
            this.detail});
            this.dataGridViewReport.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewReport.Location = new System.Drawing.Point(0, 73);
            this.dataGridViewReport.Name = "dataGridViewReport";
            this.dataGridViewReport.RowHeadersVisible = false;
            this.dataGridViewReport.Size = new System.Drawing.Size(974, 350);
            this.dataGridViewReport.TabIndex = 4;
            this.dataGridViewReport.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewReport_CellContentClick);
            // 
            // reportID
            // 
            this.reportID.HeaderText = "Report ID";
            this.reportID.Name = "reportID";
            // 
            // title
            // 
            this.title.HeaderText = "Title";
            this.title.Name = "title";
            // 
            // content
            // 
            this.content.HeaderText = "Content";
            this.content.Name = "content";
            // 
            // time
            // 
            this.time.HeaderText = "Time";
            this.time.Name = "time";
            // 
            // user
            // 
            this.user.HeaderText = "Username";
            this.user.Name = "user";
            // 
            // detail
            // 
            this.detail.HeaderText = "View Detail";
            this.detail.Name = "detail";
            this.detail.Text = "Detail";
            this.detail.UseColumnTextForLinkValue = true;
            // 
            // textBoxUserReport
            // 
            this.textBoxUserReport.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxUserReport.Location = new System.Drawing.Point(250, 22);
            this.textBoxUserReport.Name = "textBoxUserReport";
            this.textBoxUserReport.Size = new System.Drawing.Size(200, 23);
            this.textBoxUserReport.TabIndex = 3;
            this.textBoxUserReport.TextChanged += new System.EventHandler(this.textBoxUserReport_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(100, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(139, 17);
            this.label14.TabIndex = 2;
            this.label14.Text = "Search by username";
            // 
            // tabPageProfile
            // 
            this.tabPageProfile.Controls.Add(this.textBoxGenderPro);
            this.tabPageProfile.Controls.Add(this.buttonUpdateProfile);
            this.tabPageProfile.Controls.Add(this.buttonChangePass);
            this.tabPageProfile.Controls.Add(this.textBoxRePass);
            this.tabPageProfile.Controls.Add(this.textBoxNewPass);
            this.tabPageProfile.Controls.Add(this.textBoxOldPass);
            this.tabPageProfile.Controls.Add(this.textBoxUsernamePro);
            this.tabPageProfile.Controls.Add(this.label6);
            this.tabPageProfile.Controls.Add(this.label7);
            this.tabPageProfile.Controls.Add(this.label8);
            this.tabPageProfile.Controls.Add(this.label9);
            this.tabPageProfile.Controls.Add(this.dateTimePickerPro);
            this.tabPageProfile.Controls.Add(this.textBoxPositionPro);
            this.tabPageProfile.Controls.Add(this.textBoxAddressPro);
            this.tabPageProfile.Controls.Add(this.textBoxPhonePro);
            this.tabPageProfile.Controls.Add(this.label5);
            this.tabPageProfile.Controls.Add(this.label4);
            this.tabPageProfile.Controls.Add(this.label3);
            this.tabPageProfile.Controls.Add(this.label2);
            this.tabPageProfile.Controls.Add(this.label1);
            this.tabPageProfile.Controls.Add(this.textBoxNamePro);
            this.tabPageProfile.Controls.Add(this.pictureBoxAvatarPro);
            this.tabPageProfile.Location = new System.Drawing.Point(4, 26);
            this.tabPageProfile.Name = "tabPageProfile";
            this.tabPageProfile.Size = new System.Drawing.Size(974, 423);
            this.tabPageProfile.TabIndex = 3;
            this.tabPageProfile.Text = "Profile";
            this.tabPageProfile.UseVisualStyleBackColor = true;
            // 
            // textBoxGenderPro
            // 
            this.textBoxGenderPro.Enabled = false;
            this.textBoxGenderPro.Location = new System.Drawing.Point(350, 47);
            this.textBoxGenderPro.Name = "textBoxGenderPro";
            this.textBoxGenderPro.Size = new System.Drawing.Size(200, 23);
            this.textBoxGenderPro.TabIndex = 44;
            // 
            // buttonUpdateProfile
            // 
            this.buttonUpdateProfile.Location = new System.Drawing.Point(350, 347);
            this.buttonUpdateProfile.Name = "buttonUpdateProfile";
            this.buttonUpdateProfile.Size = new System.Drawing.Size(200, 27);
            this.buttonUpdateProfile.TabIndex = 43;
            this.buttonUpdateProfile.Text = "Update Profile";
            this.buttonUpdateProfile.UseVisualStyleBackColor = true;
            this.buttonUpdateProfile.Click += new System.EventHandler(this.buttonUpdateProfile_Click);
            // 
            // buttonChangePass
            // 
            this.buttonChangePass.Location = new System.Drawing.Point(750, 347);
            this.buttonChangePass.Name = "buttonChangePass";
            this.buttonChangePass.Size = new System.Drawing.Size(200, 27);
            this.buttonChangePass.TabIndex = 42;
            this.buttonChangePass.Text = "Change Password";
            this.buttonChangePass.UseVisualStyleBackColor = true;
            this.buttonChangePass.Click += new System.EventHandler(this.buttonChangePass_Click);
            // 
            // textBoxRePass
            // 
            this.textBoxRePass.Location = new System.Drawing.Point(750, 287);
            this.textBoxRePass.Name = "textBoxRePass";
            this.textBoxRePass.PasswordChar = '*';
            this.textBoxRePass.Size = new System.Drawing.Size(200, 23);
            this.textBoxRePass.TabIndex = 41;
            // 
            // textBoxNewPass
            // 
            this.textBoxNewPass.Location = new System.Drawing.Point(750, 227);
            this.textBoxNewPass.Name = "textBoxNewPass";
            this.textBoxNewPass.PasswordChar = '*';
            this.textBoxNewPass.Size = new System.Drawing.Size(200, 23);
            this.textBoxNewPass.TabIndex = 40;
            // 
            // textBoxOldPass
            // 
            this.textBoxOldPass.Location = new System.Drawing.Point(750, 167);
            this.textBoxOldPass.Name = "textBoxOldPass";
            this.textBoxOldPass.PasswordChar = '*';
            this.textBoxOldPass.Size = new System.Drawing.Size(200, 23);
            this.textBoxOldPass.TabIndex = 39;
            // 
            // textBoxUsernamePro
            // 
            this.textBoxUsernamePro.Enabled = false;
            this.textBoxUsernamePro.Location = new System.Drawing.Point(750, 107);
            this.textBoxUsernamePro.Name = "textBoxUsernamePro";
            this.textBoxUsernamePro.Size = new System.Drawing.Size(200, 23);
            this.textBoxUsernamePro.TabIndex = 38;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(600, 290);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 17);
            this.label6.TabIndex = 37;
            this.label6.Text = "Re-password";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(600, 230);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 17);
            this.label7.TabIndex = 36;
            this.label7.Text = "New password";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(600, 170);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 17);
            this.label8.TabIndex = 35;
            this.label8.Text = "Old password";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(600, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 17);
            this.label9.TabIndex = 34;
            this.label9.Text = "Username";
            // 
            // dateTimePickerPro
            // 
            this.dateTimePickerPro.CustomFormat = "ddd, dd/MM/yyyy";
            this.dateTimePickerPro.Enabled = false;
            this.dateTimePickerPro.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerPro.Location = new System.Drawing.Point(350, 107);
            this.dateTimePickerPro.Name = "dateTimePickerPro";
            this.dateTimePickerPro.Size = new System.Drawing.Size(200, 23);
            this.dateTimePickerPro.TabIndex = 33;
            // 
            // textBoxPositionPro
            // 
            this.textBoxPositionPro.Enabled = false;
            this.textBoxPositionPro.Location = new System.Drawing.Point(750, 47);
            this.textBoxPositionPro.Name = "textBoxPositionPro";
            this.textBoxPositionPro.Size = new System.Drawing.Size(200, 23);
            this.textBoxPositionPro.TabIndex = 32;
            // 
            // textBoxAddressPro
            // 
            this.textBoxAddressPro.Enabled = false;
            this.textBoxAddressPro.Location = new System.Drawing.Point(350, 227);
            this.textBoxAddressPro.Multiline = true;
            this.textBoxAddressPro.Name = "textBoxAddressPro";
            this.textBoxAddressPro.Size = new System.Drawing.Size(200, 80);
            this.textBoxAddressPro.TabIndex = 31;
            // 
            // textBoxPhonePro
            // 
            this.textBoxPhonePro.Location = new System.Drawing.Point(350, 167);
            this.textBoxPhonePro.Name = "textBoxPhonePro";
            this.textBoxPhonePro.Size = new System.Drawing.Size(200, 23);
            this.textBoxPhonePro.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(600, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 17);
            this.label5.TabIndex = 29;
            this.label5.Text = "Position";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(250, 230);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 17);
            this.label4.TabIndex = 28;
            this.label4.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(250, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 17);
            this.label3.TabIndex = 27;
            this.label3.Text = "Phone";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(250, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 17);
            this.label2.TabIndex = 26;
            this.label2.Text = "DOB";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(250, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 25;
            this.label1.Text = "Gender";
            // 
            // textBoxNamePro
            // 
            this.textBoxNamePro.Enabled = false;
            this.textBoxNamePro.Location = new System.Drawing.Point(25, 347);
            this.textBoxNamePro.Name = "textBoxNamePro";
            this.textBoxNamePro.Size = new System.Drawing.Size(200, 23);
            this.textBoxNamePro.TabIndex = 24;
            // 
            // pictureBoxAvatarPro
            // 
            this.pictureBoxAvatarPro.Location = new System.Drawing.Point(25, 50);
            this.pictureBoxAvatarPro.Name = "pictureBoxAvatarPro";
            this.pictureBoxAvatarPro.Size = new System.Drawing.Size(200, 250);
            this.pictureBoxAvatarPro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxAvatarPro.TabIndex = 23;
            this.pictureBoxAvatarPro.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // chartTrack
            // 
            chartArea4.Name = "ChartArea1";
            this.chartTrack.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.chartTrack.Legends.Add(legend4);
            this.chartTrack.Location = new System.Drawing.Point(25, 100);
            this.chartTrack.Name = "chartTrack";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Legend = "Legend1";
            series5.Name = "CheckIn";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Legend = "Legend1";
            series6.Name = "CheckOut";
            this.chartTrack.Series.Add(series5);
            this.chartTrack.Series.Add(series6);
            this.chartTrack.Size = new System.Drawing.Size(600, 300);
            this.chartTrack.TabIndex = 0;
            this.chartTrack.Text = "Track";
            // 
            // chartCard
            // 
            chartArea3.Name = "ChartArea1";
            this.chartCard.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chartCard.Legends.Add(legend3);
            this.chartCard.Location = new System.Drawing.Point(650, 100);
            this.chartCard.Name = "chartCard";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series4.Legend = "Legend1";
            series4.Name = "SeriesCard";
            this.chartCard.Series.Add(series4);
            this.chartCard.Size = new System.Drawing.Size(300, 300);
            this.chartCard.TabIndex = 1;
            this.chartCard.Text = "Card";
            // 
            // comboBoxStatistic
            // 
            this.comboBoxStatistic.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStatistic.FormattingEnabled = true;
            this.comboBoxStatistic.Items.AddRange(new object[] {
            "Week",
            "Month"});
            this.comboBoxStatistic.Location = new System.Drawing.Point(200, 22);
            this.comboBoxStatistic.Name = "comboBoxStatistic";
            this.comboBoxStatistic.Size = new System.Drawing.Size(200, 25);
            this.comboBoxStatistic.TabIndex = 41;
            this.comboBoxStatistic.SelectedIndexChanged += new System.EventHandler(this.comboBoxStatistic_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(100, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 17);
            this.label13.TabIndex = 40;
            this.label13.Text = "Statistic by";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(200, 65);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(161, 25);
            this.label20.TabIndex = 42;
            this.label20.Text = "Statistic by Track";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(700, 65);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(154, 25);
            this.label21.TabIndex = 43;
            this.label21.Text = "Statistic by Card";
            // 
            // Manager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 453);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "Manager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manager";
            this.tabControl1.ResumeLayout(false);
            this.tabPageStatistic.ResumeLayout(false);
            this.tabPageStatistic.PerformLayout();
            this.tabPageEmployee.ResumeLayout(false);
            this.tabPageEmployee.PerformLayout();
            this.groupBoxEmployee.ResumeLayout(false);
            this.groupBoxEmployee.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAvatarEmp)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPageReport.ResumeLayout(false);
            this.tabPageReport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).EndInit();
            this.tabPageProfile.ResumeLayout(false);
            this.tabPageProfile.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAvatarPro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartTrack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartCard)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageStatistic;
        private System.Windows.Forms.TabPage tabPageEmployee;
        private System.Windows.Forms.TabPage tabPageReport;
        private System.Windows.Forms.TabPage tabPageProfile;
        private System.Windows.Forms.TextBox textBoxGenderPro;
        private System.Windows.Forms.Button buttonUpdateProfile;
        private System.Windows.Forms.Button buttonChangePass;
        private System.Windows.Forms.TextBox textBoxRePass;
        private System.Windows.Forms.TextBox textBoxNewPass;
        private System.Windows.Forms.TextBox textBoxOldPass;
        private System.Windows.Forms.TextBox textBoxUsernamePro;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTimePickerPro;
        private System.Windows.Forms.TextBox textBoxPositionPro;
        private System.Windows.Forms.TextBox textBoxAddressPro;
        private System.Windows.Forms.TextBox textBoxPhonePro;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNamePro;
        private System.Windows.Forms.PictureBox pictureBoxAvatarPro;
        private System.Windows.Forms.DataGridView dataGridViewReport;
        private System.Windows.Forms.TextBox textBoxUserReport;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridViewTextBoxColumn reportID;
        private System.Windows.Forms.DataGridViewTextBoxColumn title;
        private System.Windows.Forms.DataGridViewTextBoxColumn content;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn user;
        private System.Windows.Forms.DataGridViewLinkColumn detail;
        private System.Windows.Forms.GroupBox groupBoxEmployee;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButtonFemale;
        private System.Windows.Forms.RadioButton radioButtonMale;
        private System.Windows.Forms.Button buttonDone;
        private System.Windows.Forms.TextBox textBoxNameEmp;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DateTimePicker dateTimePickerEmp;
        private System.Windows.Forms.TextBox textBoxAddressEmp;
        private System.Windows.Forms.TextBox textBoxPhoneEmp;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxSSN;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxUsernameEmp;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBoxRole;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBoxAvatarEmp;
        private System.Windows.Forms.Button buttonImage;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartCard;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartTrack;
        private System.Windows.Forms.ComboBox comboBoxStatistic;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
    }
}