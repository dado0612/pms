CREATE TABLE Role(
	roleID INT PRIMARY KEY,
	roleName VARCHAR(255) NOT NULL
)

CREATE TABLE Parking(
	parkID INT IDENTITY(1,1) PRIMARY KEY,
	parkName VARCHAR(255) NOT NULL,
	maxPlace INT NOT NULL
)

CREATE TABLE Person(
	SSN VARCHAR(255) PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	gender BIT NOT NULL,
	dob DATE NOT NULL,
	address TEXT NOT NULL,
	phone VARCHAR(255) NOT NULL
)

CREATE TABLE Plate(
	plateNumber VARCHAR(255) PRIMARY KEY,
	SSN VARCHAR(255) NOT NULL FOREIGN KEY REFERENCES Person(SSN)
)

CREATE TABLE Employee(
	username VARCHAR(255) PRIMARY KEY,
	password VARCHAR(255) NOT NULL,
	image VARCHAR(255) NOT NULL,
	roleID INT NOT NULL FOREIGN KEY REFERENCES Role(roleID),
	SSN VARCHAR(255) NOT NULL FOREIGN KEY REFERENCES Person(SSN)
)

CREATE TABLE Card(
	cardID VARCHAR(255) PRIMARY KEY,
	serialNumber INT IDENTITY(1,1),
	plateNumber VARCHAR(255),
	expireDate DATETIME NOT NULL
)

CREATE TABLE Track(
	trackID BIGINT IDENTITY(1,1) PRIMARY KEY,
	plateNumber VARCHAR(255) NOT NULL,
	status BIT NOT NULL,
	time DATETIME NOT NULL,
	image VARCHAR(255) NOT NULL,
	parkID INT NOT NULL FOREIGN KEY REFERENCES Parking(parkID),
	username VARCHAR(255) NOT NULL FOREIGN KEY REFERENCES Employee(username)
)

CREATE TABLE Report(
	reportID INT IDENTITY(1,1) PRIMARY KEY,
	trackID BIGINT NOT NULL FOREIGN KEY REFERENCES Track(trackID),
	title VARCHAR(255) NOT NULL,
	content TEXT NOT NULL,
	time DATETIME NOT NULL,
	username VARCHAR(255) NOT NULL FOREIGN KEY REFERENCES Employee(username)
)