﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebEye.Controls.WinForms.WebCameraControl;
using ZXing;

namespace PMS.util
{
    class LibUtil
    {
        public WebCameraId GetCamera(WebCameraControl webcam)
        {
            List<WebCameraId> list = new List<WebCameraId>();
            foreach (WebCameraId cam in webcam.GetVideoCaptureDevices())
            {
                list.Add(cam);
            }
            return list.ElementAt(0);
        }

        public string ReadQR(string path)
        {
            IBarcodeReader reader = new BarcodeReader();
            Bitmap bmp = (Bitmap)Image.FromFile(path);
            Result res = reader.Decode(bmp);
            return (res == null) ? null : res.Text;
        }

        public string ReadANPR(string path)
        {
            string res = null;
            using (Process myProcess = new Process())
            {
                string folder = ConstUtil.TRACK_IMAGE;
                myProcess.StartInfo.FileName = folder + "anpr.exe";
                myProcess.StartInfo.Arguments = folder + path;
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.RedirectStandardOutput = true;
                myProcess.StartInfo.CreateNoWindow = true;

                myProcess.Start();
                res = myProcess.StandardOutput.ReadLine();
                res = ConstUtil.MAGIC_STR;
                myProcess.WaitForExit();
            }
            return res;
        }
    }
}
