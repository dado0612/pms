﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PMS.util
{
    class Dummy
    {
        public string Title { get; set; }
        public string Content { get; set; }

        public Dummy(string title, string content)
        {
            Title = title;
            Content = content;
        }
    }

    class ValidUtil
    {
        public string ValidString(params Dummy[] arr)
        {
            foreach (Dummy obj in arr)
            {
                if (obj.Title.Equals(""))
                {
                    return obj.Content + " can't be empty.";
                }
            }
            return null;
        }

        public string ValidPhone(string phone)
        {
            if (phone.Equals(""))
            {
                return "Phone can't be empty.";
            }
            else
            {
                Regex regex = new Regex(@"\d{10}");
                if (phone.StartsWith("0") && regex.IsMatch(phone))
                {
                    return null;
                }
                else
                {
                    return "Invalid phone number format.";
                }
            }
        }

        public string ValidSSN(string sSN)
        {
            if (sSN.Equals(""))
            {
                return "SSN can't be empty.";
            }
            else
            {
                Regex regex = new Regex(@"\d{12}");
                return (regex.IsMatch(sSN)) ? null : "Invalid SSN format.";
            }
        }

        public string ValidImage(string image)
        {
            return (image == null) ? "Image can't be empty." : null;
        }
    }
}
