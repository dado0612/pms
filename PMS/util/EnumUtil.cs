﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.util
{
    enum STATUS
    {
        IN,
        OUT
    }

    enum ROLE
    {
        RETIRE,
        SECURITY,
        STAFF,
        MANAGER
    }
}
