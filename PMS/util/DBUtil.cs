﻿using PMS.dao;
using PMS.model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.util
{
    class DBUtil
    {
        private Random ran = new Random();
        private const int CARD_NUM = 100;  //*2.5
        private const int EMP_NUM = 25;
        private const int PARK_NUM = 4;
        private const int PERSON_NUM = 100;
        private const int PLATE_NUM = 150;

        private List<string> listSecurity = new List<string>();
        private List<string> listStaff = new List<string>();
        private List<string> listSSN = new List<string>();
        private List<string> listPlate = new List<string>();
        private List<Track> listTrack = new List<Track>();

        private string[] GetResource(string src)
        {
            string path = ConstUtil.DB_RESOURCE + src + ".txt";
            return File.ReadAllLines(path);
        }

        public void AddRole()
        {
            RoleDAO dao = new RoleDAO();
            dao.AddRole(new Role(0, "Retire"));
            dao.AddRole(new Role(1, "Security"));
            dao.AddRole(new Role(2, "Staff"));
            dao.AddRole(new Role(3, "Manager"));
            Console.WriteLine("Done role");
        }

        public void AddParking()
        {
            ParkingDAO dao = new ParkingDAO();
            for (int i = 0; i < PARK_NUM; i++)
            {
                string name = "Park " + (char)(i + 65);
                dao.AddParking(new Parking(name, 50));
            }
            Console.WriteLine("Done parking");
        }

        private string RanNum(int len)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++)
            {
                sb.Append(ran.Next(10));
            }
            return sb.ToString();
        }

        private Person RanPerson(string[] road, string name)
        {
            string sSN = RanNum(12);
            listSSN.Add(sSN);
            bool gender = (ran.Next(2) == 1);
            DateTime dob = new DateTime(ran.Next(1970, 2000), ran.Next(1, 13), ran.Next(1, 29));

            string address = " duong " + road[ran.Next(25)];
            address = "So " + ran.Next(100) + address + " TP.Ha Noi";
            string phone = "0" + RanNum(9);
            return new Person(sSN, name, gender, dob, address, phone);
        }

        public void AddEmployee()
        {
            EmployeeDAO empDAO = new EmployeeDAO();
            PersonDAO perDAO = new PersonDAO();
            string password = new EncodeUtil().EncryptPassword(ConstUtil.DEFAULT_PASSWORD);
            string[] employee = GetResource("employee");
            string[] road = GetResource("road");

            foreach (string s in employee)
            {
                string[] arr = s.Split('|');
                int roleID = ran.Next(4);

                switch (roleID)
                {
                    case (int)ROLE.SECURITY:
                        listSecurity.Add(arr[0]);
                        break;
                    case (int)ROLE.STAFF:
                        listStaff.Add(arr[0]);
                        break;
                }

                Person per = RanPerson(road, arr[1]);
                perDAO.AddPerson(per);
                string image = arr[0] + ".png";
                empDAO.AddEmployee(new Employee(arr[0], password, image, roleID, per.SSN));
            }
            Console.WriteLine("Done employee");
        }

        public void AddPerson()
        {
            PersonDAO dao = new PersonDAO();
            string[] person = GetResource("person");
            string[] road = GetResource("road");

            foreach (string s in person)
            {
                dao.AddPerson(RanPerson(road, s));
            }
            Console.WriteLine("Done person");
        }

        private string RanPlate()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 7; i++)
            {
                sb.Append(ran.Next(10));
            }

            int c = ran.Next(26) + 65;
            sb.Insert(2, (char)c);
            return sb.ToString();
        }

        public void AddPlate()
        {
            PlateDAO dao = new PlateDAO();
            for (int i = 0; i < PLATE_NUM; i++)
            {
                string plate = RanPlate();
                listPlate.Add(plate);
                string sSN = listSSN.ElementAt(ran.Next(PERSON_NUM));
                dao.AddPlate(new Plate(plate, sSN));
            }
            Console.WriteLine("Done plate");
        }

        public void AddCard()
        {
            CardDAO dao = new CardDAO();
            for (int i = 1; i <= CARD_NUM * 2.5; i++)
            {
                string cardID = new EncodeUtil().EncryptCardID(i.ToString());
                string plateNumber = "";
                DateTime expireDate;

                if (i > CARD_NUM)
                {
                    expireDate = new DateTime(2019, ran.Next(1, 4), ran.Next(1, 28),
                                        ran.Next(24), ran.Next(60), ran.Next(60));
                }
                else
                {
                    plateNumber = listPlate.ElementAt(ran.Next(PLATE_NUM));
                    expireDate = new DateTime(2019, ran.Next(1, 7), ran.Next(1, 29),
                                        ran.Next(24), ran.Next(60), ran.Next(60));
                }

                dao.AddCard(new Card(cardID, plateNumber, expireDate));
            }
            Console.WriteLine("Done card");
        }

        public void AddTrack()
        {
            string image = "demo.jpg";
            int len = listSecurity.Count;

            foreach (string s in listPlate)
            {
                int cnt = ran.Next(200);
                int status = (int)STATUS.IN;
                DateTime time = new DateTime(2019, ran.Next(1, 4), ran.Next(1, 28),
                                        ran.Next(24), ran.Next(60), ran.Next(60));
                int parkID = ran.Next(PARK_NUM) + 1;

                for (int i = 0; i < cnt && time < DateTime.Now; i++)
                {
                    string username = listSecurity.ElementAt(ran.Next(len));
                    listTrack.Add(new Track(s, status, time, image, parkID, username));
                    if (i % 2 != 0)
                    {
                        time = time.AddHours(ran.Next(20) + 20);
                        parkID = ran.Next(PARK_NUM) + 1;
                    }
                    else
                    {
                        time = time.AddHours(ran.Next(5) + 5);
                    }

                    status = 1 - status;
                    time = time.AddMinutes(ran.Next(60));
                    time = time.AddSeconds(ran.Next(60));
                }
            }

            listTrack = listTrack.OrderBy(o => o.Time).ToList();
            TrackDAO dao = new TrackDAO();
            foreach (Track track in listTrack)
            {
                dao.AddTrack(track);
            }
            Console.WriteLine("Done track");
        }

        public void AddReport()
        {
            ReportDAO dao = new ReportDAO();
            int cnt = ran.Next(100) + 100;
            int max = listTrack.Count;
            int len = listStaff.Count;

            while (cnt < max)
            {
                string username = listStaff.ElementAt(ran.Next(len));
                string add = (ran.Next(2) == 1) ? "card" : "vehicle";
                string title = "Report lost " + add;
                string content = title + " is checked by " + username;

                DateTime time = listTrack.ElementAt(cnt).Time;
                time = time.AddMinutes(ran.Next(60));
                time = time.AddSeconds(ran.Next(60));

                dao.AddReport(new Report(cnt, title, content, time, username));
                cnt += ran.Next(100) + 100;
            }
            Console.WriteLine("Done report");
        }

        public void Process()
        {
            AddRole();
            AddParking();
            AddEmployee();
            AddPerson();
            AddPlate();
            AddCard();
            AddTrack();
            AddReport();
        }
    }
}
