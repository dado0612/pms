﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.util
{
    class ConstUtil
    {
        public const string EMPLOYEE_IMAGE = "../../img/emp/";
        public const string TRACK_IMAGE = "../../img/track/";
        public const string TMP_IMAGE = "../../img/tmp/";
        public const string DB_RESOURCE = "../../db/";
        public const string DEFAULT_PASSWORD = "1";
        public const string MAGIC_STR = "30A88888";
    }
}
