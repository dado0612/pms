﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PMS.util
{
    class EncodeUtil
    {
        private string Encrypt(byte[] hash)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in hash)
            {
                sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }

        public string EncryptPassword(string password)
        {
            byte[] hash = Encoding.UTF8.GetBytes(password);
            hash = SHA256.Create().ComputeHash(hash);
            return Encrypt(hash);
        }

        public string EncryptCardID(string cardID)
        {
            byte[] hash = Encoding.UTF8.GetBytes(cardID);
            hash = MD5.Create().ComputeHash(hash);
            return Encrypt(hash);
        }
    }
}
